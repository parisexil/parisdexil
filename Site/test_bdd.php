<?php 

// Reporte toutes les erreurs PHP (Voir l'historique des modifications)
//error_reporting(E_ALL);

// Rapporte toutes les erreurs à part les E_NOTICE
// C'est la configuration par défaut de php.ini
error_reporting(E_ALL & ~E_NOTICE);


include_once('Model.php');

$tab_personne_robert = [
			'nom' => 'Dupont',
			'prenom' => 'Robert',
];

$tab_personne_kevin = [
			'nom' => 'Durand',
			'prenom' => 'Kevin',
];

$tab_personne_john = [
			'nom' => 'Doe',
			'prenom' => 'John',
];

$tab_benevole = [
					'mail' => 'test@mail.com',
					'identifiant' => 'Robert77',
					'mdp' => 'passs'
];

$tab_hebergeur = [
					'adresse' => '1 rue de la paix',
					'code_postal' => '01700',
					'ville' => 'Paris'
];

$tab_mineur = [
					'age' => '17',
					'date_arrive' => '2018-07-25',
					'genre' => true,
];

$tab_transfert = [
					'num_mineur_id' => '1',
					'num_hebergeur_id' => '1',
					'date_debut' => '2019-01-01',
					'date_fin' => '2019-02-01',
					'etat' => 'aVenir'
];

$tab_benevole['mdp'] = hash('sha512', $tab_benevole['mdp']);

//var_dump($tab_benevole['mdp']);
//print_r($tab_benevole);

//var_dump($m->addPersonne($tab_personne));
var_dump($m->addBenevole($tab_personne_john, $tab_benevole));
var_dump($m->addMineur($tab_personne_kevin, $tab_mineur));
var_dump($m->addHebergeur($tab_personne_robert, $tab_hebergeur));
var_dump($m->addTransfert($tab_transfert));


?>