
<?php $title="Bénévoles"; require_once("../header.php") ;?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/img-01.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Bénévoles</span>
							<h1>Les Bénévoles</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>	
	
	<div id="gtco-features" class="border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8">
					<p><a href="./ajoutBenevole.php" class="btn btn-default btn-block">Ajouter un bénévole</a></p>
				</div>
				<div class="col-6 col-md-4">
				    <form class="text-center" role="search"> 
					    <div class="form-inline"> 
					    	<input type="text" class="form-control mr-sm-2" placeholder="Search" /> 
					     	<span class="input-group-btn"> 
					      		<button type="button" class="btn btn-outline my-2 my-sm-0"><i class="fas fa-search"></i></button> 
					     	</span> 
					    </div> 
				    </form> 
			    </div>
			</div>

			<div class="row">

				<?php for($i=0; $i<sizeof($benevoles); $i++): ?>

					<div class="col-md-3 col-sm-6">
						<div class="feature-center animate-box" data-animate-effect="fadeIn">
							<span class="icon">
								<img src="/parisdexil/ressources/images/no-image.jpg" height="200" width="175"/>
							</span>
							<h3><?= $benevoles[$i]['nom_benevole']?></h3>
							<p>
								<ul class="text-left">
									<li>Date d'arrivée : <?= $benevoles[$i]['dateArrivee']?></li>
								</ul>
							</p>
							<hr>
							<?php for($j=0; $j<sizeof($benevoles[$i]['tags']); $j++): ?>
								<span class="badge badge-info"><?= $benevoles[$i]['tags'][$j] ?></span>
							<?php endfor; ?>
							<hr>
							<p><a href="./infoBenevoles?id=<?= $benevoles[$i]['id']?>" class="btn btn-default btn-block">Plus d'informations</a></p>
						</div>
					</div>

				<?php endfor; ?>

			</div>

			<nav aria-label="Page navigation example" class="col-lg-6 offset-lg-3 py-5">
			  	<ul class="pagination">
			    	<li class="page-item disabled">
				      	<a class="page-link" href="#" aria-label="Previous">
				        	<span aria-hidden="true">&laquo;</span>
				      	</a>
			   		</li>
				    <li class="page-item active"><a class="page-link" href="#">1</a></li>
				    <li class="page-item"><a class="page-link" href="#">2</a></li>
				    <li class="page-item"><a class="page-link" href="#">3</a></li>
				    <li class="page-item">
					    <a class="page-link" href="#" aria-label="Next">
					    	<span aria-hidden="true">&raquo;</span>
					    </a>
			    	</li>
			  	</ul>
			</nav>

		</div>
	</div>
	
<?php require_once('../footer.html') ?>