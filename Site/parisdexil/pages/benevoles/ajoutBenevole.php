
<?php $title="Ajouter un Bénévole"; require_once("../header.php"); ?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/img_4.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small"></span>
							<h1>Ajout d'un bénévole</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	
	<div class="gtco-section">
		<div>
			<form class="form" action="./ajout.php" method="post">
				<label>Informations Personnelles</label>
				<hr>
				<div class="form-group">
					<br>
					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="nom">Nom <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="nom" name="nom">
						</div>

						<div class="form-group col-md-5">
							<label for="prenom">Prénom <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="prenom" name="prenom">
						</div>
						<div class="form-group col-md-2">
							<label for="Genre">Genre <span class="obligatoire">*</span></label>
							<select id="Genre" class="form-control" name="genre">
								<option selected>Choisir ...</option>
								<option>Masculin</option>
								<option>Féminin</option>
							</select>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="tel">Téléphone</label>
							<input type="tel" class="form-control" id="tel" name="tel">
						</div>

						<div class="form-group col-md-8">
							<label for="autrecontact">Autre Contact ( <span class="obligatoire">*</span> si pas de téléphone)</label>
							<input type="text" class="form-control" id="autrecontact" name="autrecontact">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="description">Description</label>
							<textarea id="description" class="form-control" name="description" rows="5" ></textarea>
						</div>
					</div>
					<br>
				</div>
				<br>

				<label>Informations Compte</label>
				<hr>
				<div class="form-group">
					<br>
					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="mail">Adresse Mail <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="mail" name="mail">
						</div>

						<div class="form-group col-md-5">
							<label for="identifiant">Identifiant <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="identifiant" name="identifiant">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="mdp">Mot de passe <span class="obligatoire">*</span></label>
							<input type="password" class="form-control" id="mdp" name="mdp">
						</div>
					</div>
				</div>

				<div class="form-row">
						<div class="form-group col-md-3">
							<label>Droit Administrateur <span class="obligatoire">*</span></label>
							<hr>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="admin" id="adminoui" value=true checked>
							  	<label class="form-check-label" for="adminoui">
							    	Oui
							  	</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="admin" id="adminnon" value=false>
							  	<label class="form-check-label" for="adminnon">
							    	Non
							  	</label>
							</div>
						</div>
				</div>
				<br>

				<label>Informations Supplémentaires</label>
				<hr>
				<div class="form-group">
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="remarque">Remarques</label>
							<textarea id="remarque" class="form-control" name="remarque" rows="5" ></textarea>
						</div>
					</div>
			  	</div>
			  	<br>
			  	<div class="form-row">
					<button type="submit" class="btn btn-primary">Ajouter</button>
				</div>
			</form>
		</div>
	</div>

<?php require_once("../footer.html") ;?>