<?php
session_start();

if (!isset($_SESSION['connected'])) {
	$_SESSION['connected'] = false;

}

if (!$_SESSION['connected']) {

	if (isset($_POST['username']) and isset($_POST['password'])) {
		$username = $_POST['username'];
		$password = $_POST['password'];

	} else {
		header('Location: http://localhost/parisdexil/index.html');
		exit();
	}

	$login = $m->login($username, $password);

	if ($login['valide']) {
		$info = $m->getBenevoleById($login['id_b']);

		if ($info['actif'] == 0) {
			header('Location: http://localhost/parisdexil/index.html');
			exit();
		}

		$_SESSION['connected'] = true;
		$_SESSION['nom'] = $info['nom'];
		$_SESSION['prenom'] = $info['prenom'];
		$_SESSION['id_b'] = $login['id_b'];
		$_SESSION['admin'] = ($info['admin'] == 1);

	} else {
		header('Location: http://localhost/parisdexil/index.html');
		exit();
	}
}

?>
