
# Paris D'exil

## Mise en place de l'environnement de dev

- Créer une base de données nommer `parisdexil`
- Modifier la ligne suivante du fichier `.env` avec vos infos:
`DATABASE_URL=mysql://user:mdp@127.0.0.1:3306/parisdexil`

- Taper cette commande dans la console:
` php bin/console doctrine:migrations:migrate`

- La base a été généré
