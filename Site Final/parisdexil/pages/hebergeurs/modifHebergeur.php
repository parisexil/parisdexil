
<?php $title="Modifier un Hébergeur"; require_once("../header.php");
	  $hebergeur=$m->getHebergeurById($_GET['id']);
	  $tags=unserialize(base64_decode($hebergeur['tags']));
	  $disponibilite=unserialize(base64_decode($hebergeur['disponibilite'])); ?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/hebergeurs.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small"></span>
							<h1>Modifier un Hébergeur</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	
	<div class="gtco-section">
		<div>
			<form class="form" action="./modif.php" method="post">
				<input name="id" value="<?= $_GET['id'] ?>" hidden/>
				<input name="id_p" value="<?= $_GET['id_p'] ?>" hidden/>
				<label>Informations Personnelles</label>
				<hr>
				<div class="form-group">
					<br>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="nom">Nom <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="nom" name="nom" value="<?= $hebergeur['nom'] ?>">
						</div>

						<div class="form-group col-md-6">
							<label for="prenom">Prénom <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="prenom" name="prenom" value="<?= $hebergeur['prenom'] ?>">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="tel">Téléphone <span class="obligatoire">*</span></label>
							<input type="tel" class="form-control" id="tel" name="tel" value="<?= $hebergeur['telephone'] ?>">
						</div>

						<div class="form-group col-md-8">
							<label for="autrecontact">Autre Contact (si pas de téléphone) <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="autrecontact" name="autrecontact"value="<?= $hebergeur['autre_contact'] ?>">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-7">
							<label for="adresse">Adresse <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="adresse" name="adresse" value="<?= $hebergeur['adresse'] ?>">
						</div>
						<div class="form-group col-md-3">
							<label for="ville">Ville <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="ville" name="ville" value="<?= $hebergeur['ville'] ?>">
						</div>
						<div class="form-group col-md-2">
							<label for="codepostal">Code Postal <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="codepostal" name="codepostal" value="<?= $hebergeur['code_postal'] ?>">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="description">Description</label>
							<textarea id="description" class="form-control" name="description" rows="5" ><?= $hebergeur['description'] ?></textarea>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="description">Description Endroit</label>
							<textarea id="description" class="form-control" name="descriptionEndroit" rows="5" ><?= $hebergeur['description_endroit'] ?></textarea>
						</div>
					</div>

					<br>
					<div class="form-row">
						<div class="form-group col-md-3">
							<label>Donne la clé ou pas <span class="obligatoire">*</span></label>
							<hr>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="cle" id="cle" value="Donne la clé" <?= $tags[0]=="Donne la clé" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="cle">
							    	Donne la clé
							  	</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="cle" id="noncle" value="Ne donne pas la clé" <?= $tags[0]!="Donne la clé" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="noncle">
							    	Ne donne pas la clé
							  	</label>
							</div>
						</div>

						<div class="form-group col-md-1"></div>

						<div class="form-group col-md-4">
							<label>Acceuille qui ? <span class="obligatoire">*</span></label>
							<hr>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="accueilqui" id="fille" value="Fille" <?= $tags[1]=="Accueille que des filles" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="fille">
							    	Accueille que des filles
							  	</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="accueilqui" id="garçon" value="Garçon" <?= $hebergeur['tags'][1]=="Accueille que des garçons" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="garçon">
							    	Accueille que des garçons
							  	</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="accueilqui" id="jeunes" value="Jeunes Autonomes" <?= $tags[1]=="Accueille que les jeunes autonomes" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="jeunes">
							    	Accueille que les jeunes autonomes
							  	</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="accueilqui" id="pasdepreference" value="Pas de préférence" <?= $tags[1]=="Pas de préférence" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="pasdepreference">
							    	Pas de préférence
							  	</label>
							</div>
						</div>

						<div class="form-group col-md-1"></div>

						<div class="form-group col-md-3">
							<label>Hébergement <span class="obligatoire">*</span></label>
							<hr>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="habitat" id="foyer" value="Héberge dans le foyer" <?= $tags[2]=="Héberge dans le foyer" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="foyer">
							    	Héberge dans le foyer
							  	</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="habitat" id="pret" value="Prêt d'appartement" <?= $tags[2]=="Prêt d'appartement" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="pret">
							    	Prêt d'appartement
							  	</label>
							</div>
						</div>
					</div>

					<br>
					<label>Disponibilités</label>
					<hr>
					<div class="form-group" id="periode">
						<div class="form-row">
							<div class="form-group col-md-2">
								<div class="btn" id="ajoutPeriode">Ajouter une période</div>
							</div>
						</div>
						<?php for($i=0; $i<sizeof($disponibilite); $i++): ?>
							<div class="form-row">
								<div class="form-group col-md-5">
									<label for="datededebut">Date début</label>
									<input type="date" class="form-control" name="datededebut0" id="datededebut" value="<?= $disponibilite[$i][0] ?>">
								</div>

								<div class="form-group col-md-5">
									<label for="datedefin">Date de fin</label>
									<input type="date" class="form-control" name="datedefin0" id="datedefin" value="<?= $disponibilite[$i][1] ?>">
								</div>
								<div class="form-group col-md-1">
									<br>
									<i class="fas fa-times fa-2x" id="suppPer"></i>
								</div>
							</div>
						<?php endfor; ?>
					</div>
				</div>
				<br>

				<label>Informations Supplémentaires</label>
				<hr>
				<div class="form-group">
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="remarque">Remarques</label>
							<textarea id="remarque" class="form-control" name="remarque" rows="5" ><?= $hebergeur['remarque'] ?></textarea>
						</div>
					</div>
			  	</div>
			  	<br>
			  	<div class="form-row">
					<button type="submit" class="btn btn-primary">Modifier</button>
				</div>
			</form>
		</div>
	</div>

<?php require_once("../footer.html") ;?>