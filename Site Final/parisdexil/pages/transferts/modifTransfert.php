
<?php $title="Modifier un Transfert"; require_once("../header.php");
	  $jeunes= $m->getAllMineurs();
	  $hebergeurs= $m->getAllHebergeurs();
	  $transfert=$m->getTransfertById($_GET['id']);  ?>

	
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/transferts.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small"></span>
							<h1>Modifier un Transfert</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	<div class="gtco-section">
		<div>
			<form class="form" action="./modif.php" method="post">
				<input name="id" value="<?= $_GET['id'] ?>" hidden/>
				<label>Jeune</label>
				<hr>
				<div class="form-group">
					<br>
					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="jeunes">Nom du Jeune<span class="obligatoire">*</span></label>
							<select id="jeunes" class="form-control" name="id_jeune">
								<option >Choisir ...</option>
								<?php for($i=0; $i<sizeof($jeunes); $i++) : ?>
									<option <?= ($jeunes[$i]['nom']==$transfert['nom_m']) ? "selected" : " " ?> value="<?= $jeunes[$i]['id'] ?>" > <?= $jeunes[$i]['nom'].' '.$jeunes[$i]['prenom']?> </option>
								<?php endfor; ?>
							</select>
						</div>
					</div>
				</div>
				<br>

				<label>Hébergeur</label>
				<hr>
				<div class="form-group">
					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="hebergeur">Nom du Hébergeur<span class="obligatoire">*</span></label>
							<select id="hebergeur" class="form-control" name="id_hebergeur">
								<option >Choisir ...</option>
								<?php for($j=0; $j<sizeof($hebergeurs); $j++) : ?>
									<option <?= ($hebergeurs[$j]['nom']==$transfert['nom_h']) ? "selected" : " " ?> value="<?= $hebergeurs[$j]['id'] ?>" > <?= $hebergeurs[$j]['nom'].' '.$hebergeurs[$j]['prenom'] ?> </option>
								<?php endfor; ?>
							</select>
						</div>
					</div>
			  	</div>
			  	<br>
			  	<br>

				<label>Transfert</label>
				<hr>
				<div class="form-group">
					<div class="form-row">
						<div class="form-group col-md-2">
							<label for="dateDebut">Date de début <span class="obligatoire">*</span></label>
							<input type="date" class="form-control" id="dateDebut" name="dateDebut" value="<?= $transfert['date_debut'] ?>">
						</div>
					</div>
					<br>
					<div class="form-row">
						<div class="form-group col-md-2">
							<label for="dateFin">Date de fin (si connue)</label>
							<input type="date" class="form-control" id="dateFin" name="dateFin" value="<?= $transfert['date_fin'] ?>">
						</div>
					</div>
					<br>
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="status">Status du Transfert<span class="obligatoire">*</span></label>
							<select id="status" class="form-control" name="status">
								<option >Choisir ...</option>
								<option value="enCours" <?= $transfert['etat']=="enCours" ? "selected" : " " ?>>En cours</option>
								<option value="aVenir" <?= $transfert['etat']=="aVenir" ? "selected" : " " ?>>A venir</option>
								<option value="annulé" <?= $transfert['etat']=="annulé" ? "selected" : " " ?>>Annulé</option>
								<option value="terminé" <?= $transfert['etat']=="terminé" ? "selected" : " " ?>>Terminé</option>
							</select>
						</div>
					</div>
			  	</div>
			  	<br>
			  	<br>

			  	<div class="form-row">
					<button type="submit" class="btn btn-primary">Modifier</button>
				</div>
			</form>
		</div>
	</div>

<?php require_once("../footer.html") ;?>