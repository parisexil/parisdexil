
<?php $title = "Bénévoles";require_once "../header.php";
$benevoles = $m->getAllBenevole();?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/benevoles.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Bénévoles</span>
							<h1>Les Bénévoles</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="gtco-features" class="border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12">
					<p><a href="./ajoutBenevole.php" class="btn btn-default btn-block">Ajouter un bénévole</a></p>
				</div>
				<!-- <div class="col-6 col-md-4">
				    <form class="text-center" role="search">
					    <div class="form-inline">
					    	<input type="text" class="form-control mr-sm-2" placeholder="Search" />
					     	<span class="input-group-btn">
					      		<button type="button" class="btn btn-outline my-2 my-sm-0"><i class="fas fa-search"></i></button>
					     	</span>
					    </div>
				    </form>
			    </div> -->
			</div>

			<div class="row">

				<?php for ($i = 0; $i < sizeof($benevoles); $i++): ?>

					<?php if ($benevoles[$i]['actif'] == '1'): ?>

						<div class="col-md-3 col-sm-6">
							<div class="feature-center animate-box" data-animate-effect="fadeIn">
								<span class="icon">
									<img src="/parisdexil/ressources/images/photo-benevole.png" height="200" width="175"/>
								</span>
								<h3><?=$benevoles[$i]['nom'] . ' ' . $benevoles[$i]['prenom']?></h3>
								<p>
									<ul class="text-left">
										<li>Date d'ajout : <?=$benevoles[$i]['date_ajout']?></li>
									</ul>
								</p>
								<hr>
								<span class="badge badge-light"><?=$benevoles[$i]['admin'] == "1" ? "Admin" : " "?></span>
								<hr>
								<p><a href="./infoBenevole.php?id=<?=$benevoles[$i]['id']?>" class="btn btn-default btn-block">Plus d'informations</a></p>
							</div>
						</div>

					<?php endif;?>

				<?php endfor;?>

			</div>
		</div>
	</div>

<?php require_once '../footer.html'?>