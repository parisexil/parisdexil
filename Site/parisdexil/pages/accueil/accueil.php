
<?php $title="Accueil"; require_once("../header.php"); ?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/img_4.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Bienvenue</span>
							<h1><?= $nomBenevole ?></h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>


	<div class="gtco-section border-bottom">	<!-- Début partie des catégories -->
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Catégories</h2>
					<p>Vous trouverez ci-dessous les différentes catégories.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="../jeunes/jeunes.php" class="fh5co-project-item">
						<figure>
							<div class="overlay"><i class="fas fa-eye"></i></div>
							<img src="/parisdexil/ressources/images/img_2.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>Jeunes</h2>
							<p>Vous trouverez ici la liste des jeunes.</p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="../hebergeurs/hebergeurs.php" class="fh5co-project-item">
						<figure>
							<div class="overlay"><i class="fas fa-eye"></i></div>
							<img src="/parisdexil/ressources/images/house.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>Hébergeurs</h2>
							<p>Vous trouverez ici la liste des hébergeurs.</p>
						</div>
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<a href="../transferts/transferts.php" class="fh5co-project-item">
						<figure>
							<div class="overlay"><i class="fas fa-eye"></i></div>
							<img src="/parisdexil/ressources/images/transf.jpg" alt="Image" class="img-responsive">
						</figure>
						<div class="fh5co-text">
							<h2>Transferts</h2>
							<p>Vous trouverez ici la liste des transferts.</p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>	<!-- Fin partie des catégories -->

	
	<div id="gtco-counter" class="gtco-section">	<!-- Début partie des Chiffres -->
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading animate-box">
					<h2>Chiffres</h2>
					<p>Vous trouverez ici des chiffres sur le nombre de jeunes, d'hébergeurs,de transferts en cours et de bénévoles.</p>
				</div>
			</div>

			<div class="row">
				
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="fas fa-user fa-2x"></i>
						</span>
						<span class="counter js-counter" data-from="0" data-to="<?= $m->getNbMineur(); ?>" data-speed="1000" data-refresh-interval="50">1</span>
						<span class="counter-label">Jeunes</span>

					</div>
				</div>
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="far fa-user fa-2x"></i>
						</span>
						<span class="counter js-counter" data-from="0" data-to="<?= $m->getNbHebergeur(); ?>" data-speed="1000" data-refresh-interval="50">1</span>
						<span class="counter-label">Hébergeurs</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="fas fa-exchange-alt fa-2x"></i>
						</span>
						<span class="counter js-counter" data-from="0" data-to="<?= $m->getNbTransfert(); ?>" data-speed="1000" data-refresh-interval="50">1</span>
						<span class="counter-label">Transferts en cours</span>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 animate-box" data-animate-effect="fadeInLeft">
					<div class="feature-center">
						<span class="icon">
							<i class="fas fa-users fa-2x"></i>
						</span>
						<span class="counter js-counter" data-from="0" data-to="<?= $m->getNbBenevole(); ?>" data-speed="1000" data-refresh-interval="50">1</span>
						<span class="counter-label">Bénévole</span>

					</div>
				</div>	
			</div>
		</div>
	</div>	<!-- Fin Partie des Chiffres -->

<?php require_once("../footer.html") ;?>
