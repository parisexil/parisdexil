
<?php $title="Informations Hébergeur"; require_once("../header.php");
	  $hebergeur=$m->getHebergeurById($_GET['id']);
	  $disponibilite=unserialize(base64_decode($hebergeur['disponibilite']));
	  $transfert=$m->getAllTransfertForHebergeur($_GET['id']); ?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/hebergeurs.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Informations Hébergeur</span>
							<h1><?= $hebergeur['nom']." ".$hebergeur['prenom'] ?></h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	<div id="gtco-features" class="border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-6 col-md-4">
				    <form class="text-center" role="search"> 
					   
				    </form> 
			    </div>
			</div>
			
			<div class="info">
				<div class="row">
					<div class="col-sm">
						<div class="feature-center animate-box" data-animate-effect="fadeIn">
							<span class="icon">
								<img src="/parisdexil/ressources/images/photo-hebergeur.png" height="200" width="175"/>
							</span>
							<h3><?= $hebergeur['nom']." ".$hebergeur['prenom'] ?></h3>
							<p>
								<ul class="text-left">
									<li>Télephone : <span><?= $hebergeur['telephone'] ?></span> </li>
									<li>Autre Contact : <span><?= $hebergeur['autre_contact'] ?></span> </li>
									<li>Adresse : <span><?= $hebergeur['adresse'] ?></span></li>
									<li>Ville : <span><?= $hebergeur['ville'] ?> </span></li>
									<li>Code Postal : <span><?= $hebergeur['code_postal'] ?></span></li>
									<li>Description : <span><?= $hebergeur['description'] ?></span> </li>
									<li>Description du lieu de logement : <span><?= $hebergeur['description_endroit'] ?></span> </li>
									<li>Clé : <span><?= unserialize(base64_decode($hebergeur['tags']))[0] ?></span> </li>
									<li>Type d'accueil : <span><?= unserialize(base64_decode($hebergeur['tags']))[1] ?></span> </li>
									<li>Type d'hébergement : <span><?= unserialize(base64_decode($hebergeur['tags']))[2] ?></span> </li>
									<li>Disponibilités : </li>
									<br>
									<table class="table table-hover">
		                                <thead class="thead-dark">
		                                    <tr>
		                                        <th scope="col">Période</th>
		                                        <th scope="col">Date de début</th>
		                                        <th scope="col">Date de fin</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
		                                	<?php for($i=0; $i<sizeof($disponibilite); $i++ ): ?>
			                                    <tr>
			                                        <th scope="row"><?= $i ?></th>
			                                        <td><?= $disponibilite[$i][0] ?></td>
			                                        <td><?= $disponibilite[$i][1] ?></td>
			                                        
			                                    </tr>
		                                	<?php endfor; ?>
		                                </tbody>
		                            </table>
		                            <br>
									<li>Remarque : <span><?= $hebergeur['remarque'] ?></span> </li>
								</ul>
							</p>
							<hr>
							<h3>Historique Transfert</h3>
                            <br>
                            <table class="table table-hover">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Date de début</th>
                                        <th scope="col">Date de fin</th>
                                        <th scope="col">Nom Jeune</th>
                                        <th scope="col">Etat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<?php for($i=0; $i<sizeof($transfert); $i++ ): ?>
	                                    <tr>
	                                        <th scope="row"><?= $i ?></th>
	                                        <td><?= $transfert[$i]['date_debut'] ?></td>
	                                        <td><?= $transfert[$i]['date_fin'] ?></td>
	                                        <td><?= $transfert[$i]['nom_m']." ".$transfert[$i]['prenom_m'] ?></td>
	                                        <td><?= $transfert[$i]['etat'] ?></td>
	                                    </tr>
                                	<?php endfor; ?>
                                </tbody>
                            </table>
							<hr>
							<div class="form-row">
								<p class="col-sm-6 "><a href="./modifHebergeur.php?id=<?= $hebergeur['id'] ?>&id_p=<?= $hebergeur['num_personne_id'] ?>" class="btn btn-default btn-block">Modifier</a></p>
								<p class="col-md-6 	"><a href="./suppHebergeur.php?id=<?= $hebergeur['num_personne_id'] ?>" class="btn btn-danger btn-block">Supprimer</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php require_once('../footer.html');?>