
<?php $title="Informations Bénévole"; require_once("../header.php") ;?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/img_4.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Informations Bénévole</span>
							<h1><?= $nom_benevole ?></h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	
	<div id="gtco-features" class="border-bottom">
		<div class="gtco-container">
			<div class="info">
				<div class="row">
					<div class="col-sm">
						<div class="feature-center animate-box" data-animate-effect="fadeIn">
							<span class="icon">
								<img src="/parisdexil/ressources/images/no-image.jpg" height="200" width="175"/>
							</span>
							<h3><?= $nom_benevole ?></h3>
							<p>
								<ul class="text-left">
									<li>Genre : <span><?= $genre ?></span> </li>
									<li>Télephone : <span><?= $tel ?></span> </li>
									<li>Autre Contact : <span><?= $autrecontact ?></span> </li>
									<li>Mail : <span><?= $mail ?></span> </li>
									<li>Description : <span><?= $description ?></span> </li>
									<li>Date d'ajout : <span><?= $dateAjout ?></span> </li>
									<li>Remarque : <span><?= $remarque ?></span> </li>
								</ul>
							</p>
							<hr>
							<div class="form-row">
								<p class="col-sm-6 "><a href="./modifBenevole.php?id=<?= $id ?>" class="btn btn-default btn-block">Modifier</a></p>
								<p class="col-md-6 	"><a href="./suppBenevole.php?id=<?= $id ?>" class="btn btn-danger btn-block">Supprimer</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php require_once('../footer.html') ?>