
<?php $title="Informations Jeune"; require_once("../header.php"); 
      $jeunes=$m->getMineurById($_GET['id']);
      $tags=unserialize(base64_decode($jeunes['tags']));
      $transfert=$m->getAllTransfertForMineur($_GET['id']); ?>

    <header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/jeunes.jpg')">
        <div class="overlay"></div>
        <div class="gtco-container">
            <div class="row">
                <div class="col-md-12 col-md-offset-0 text-left">
                    <div class="row row-mt-15em">
                        <div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
                            <span class="intro-text-small">Informations</span>
                            <h1><?= $jeunes['nom']." ".$jeunes['prenom'] ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="gtco-features" class="border-bottom">
        <div class="gtco-container">
            <div class="row">
                <div class="col-6 col-md-4">
                    <form class="text-center" role="search">

                    </form>
                </div>
            </div>

            <div class="info">
                <div class="row">
                    <div class="col-sm">
                        <div class="feature-center animate-box" data-animate-effect="fadeIn">
                            <span class="icon">
										<img src="/parisdexil/ressources/images/photo-jeune.png" height="200" width="175"/>
									</span>
                            <h3><?= $jeunes['nom']." ".$jeunes['prenom'] ?></h3>
                            <p>
                                <ul class="text-left">
                                    <li>Age : <span><?= $jeunes['age'] ?></span> </li>
                                    <li>Genre : <span><?= $jeunes['genre']==1 ? "Garçon" : "Fille" ?></span> </li>
                                    <li>Téléphone : <span><?= $jeunes['telephone'] ?></span> </li>
                                    <li>Autre Contact : <span> <?= $jeunes['autre_contact'] ?></span> </li>
                                    <li>Description : <span><?= $jeunes['description'] ?></span> </li>

                                    <li>Categorie : <span><?= $tags[1] ?></span> </li>
                                    <li>Remarque : <span><?= $jeunes['remarque'] ?></span> </li>

                                </ul>
                            </p>
                            <hr>
                            <h3>Historique Transfert</h3>
                            <br>
                            <table class="table table-hover">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Date de début</th>
                                        <th scope="col">Date de fin</th>
                                        <th scope="col">Nom hébergeur</th>
                                        <th scope="col">Etat</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0; $i<sizeof($transfert); $i++ ): ?>
                                        <tr>
                                            <th scope="row"><?= $i ?></th>
                                            <td><?= $transfert[$i]['date_debut'] ?></td>
                                            <td><?= $transfert[$i]['date_fin'] ?></td>
                                            <td><?= $transfert[$i]['nom_h']." ".$transfert[$i]['prenom_h'] ?></td>
                                            <td><?= $transfert[$i]['etat'] ?></td>
                                        </tr>
                                    <?php endfor; ?>
                                </tbody>
                            </table>
                            <hr>
                            <div class="form-row">
                                <p class="col-sm-6 "><a href="./modifJeune.php?id=<?= $jeunes['id'] ?>&id_p=<?= $jeunes['num_personne_id']  ?>" class="btn btn-default btn-block">Modifier</a></p>
                                <p class="col-md-6  "><a href="./suppJeune.php?id=<?= $jeunes['num_personne_id'] ?>" class="btn btn-danger btn-block">Supprimer</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require_once("../footer.html") ;?>