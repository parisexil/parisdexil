<?php require_once("../../ressources/Model/Model.php");
	
	if ( isset($_POST['nom'],
			   $_POST['prenom'],
			   $_POST['genre'],
			   $_POST['mail'],
			   $_POST['identifiant'],
			   $_POST['mdp'])
		&& (isset($_POST['tel']) || isset($_POST['autrecontact']))
		)
	{
		// Information Personne
		$nom=htmlspecialchars($_POST['nom']);
		$prenom=htmlspecialchars($_POST['prenom']);
		$genre=htmlspecialchars($_POST['genre']);
		$tel=htmlspecialchars($_POST['tel']);
		$autrecontact=htmlspecialchars($_POST['autrecontact']);
		$description=htmlspecialchars($_POST['description']);
		$remarque=htmlspecialchars($_POST['remarque']);


		$infos_personne=[
						  'nom' => $nom,
						  'prenom' => $prenom,
						  'telephone' => $tel,
						  'autre_contact' => $autrecontact,
						  'description' => $description,
						  'remarque' => $remarque,
						  'tags' => [],
						  'actif' => true
		];

		// Information benevole
		$mail=htmlspecialchars($_POST['mail']);
		$identifiant=htmlspecialchars($_POST['identifiant']);
		$mdp=htmlspecialchars($_POST['mdp']);
		$admin=htmlspecialchars($_POST['admin']);

		$infos_benevole=[
						   'mail' => $mail,
						   'identifiant' => $identifiant,
						   'mdp' => $mdp,
						   'admin' => $admin
		];

		$m->addBenevole($infos_personne, $infos_benevole);

		header('Location: ./benevoles.php');
  		exit();
	}
	else
	{
		header('Location: ./ajoutBenevole.php');
  		exit();
	}

?>