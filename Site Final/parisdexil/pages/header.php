<?php
require_once "../../ressources/Model/Model.php";
require_once "../securite/connexion.php";
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?=$title?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="ParisD'Exil" />
		<meta property="og:title" content=""/>
		<meta property="og:image" content=""/>
		<meta property="og:url" content=""/>
		<meta property="og:site_name" content=""/>
		<meta property="og:description" content=""/>
		<meta name="twitter:title" content="" />
		<meta name="twitter:image" content="" />
		<meta name="twitter:url" content="" />
		<meta name="twitter:card" content="" />

		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

		<!-- Animate.css -->
		<link rel="stylesheet" href="/parisdexil/ressources/css/animate.css">
		<!-- Icomoon Icon Fonts-->
		<link rel="stylesheet" href="/parisdexil/ressources/css/icomoon.css">
		<!-- Themify Icons-->
		<link rel="stylesheet" href="/parisdexil/ressources/css/themify-icons.css">
		<!-- Bootstrap  -->
		<link rel="stylesheet" href="/parisdexil/ressources/css/bootstrap.css">

		<link rel="stylesheet" href="/parisdexil/ressources/css/bootstrap2.css">
		<!-- Font Awesome -->

		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

		<!-- Magnific Popup -->
		<link rel="stylesheet" href="/parisdexil/ressources/css/magnific-popup.css">

		<!-- Owl Carousel  -->
		<link rel="stylesheet" href="/parisdexil/ressources/css/owl.carousel.min.css">
		<link rel="stylesheet" href="/parisdexil/ressources/css/owl.theme.default.min.css">

		<!-- Theme style  -->
		<link rel="stylesheet" href="/parisdexil/ressources/css/style.css">

		<!-- Modernizr JS -->
		<script src="/parisdexil/ressources/js/modernizr-2.6.2.min.js"></script>
		<!-- FOR IE9 below -->
		<!--[if lt IE 9]>
		<script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="gtco-loader"></div>

		<div id="page">
		    <div class="page-inner">
		        <nav class="gtco-nav" role="navigation">
		            <div class="gtco-container">
		                <div class="row">
		                    <div class="col-sm-4 col-xs-12">
		                        <div id="gtco-logo"><a href="/parisdexil/pages/accueil/accueil.php">parisdexil<em>.</em></a></div>
		                    </div>
		                    <div class="col-xs-8 text-right menu-1">
		                        <ul>
		                            <li><a href="/parisdexil/pages/jeunes/jeunes.php">Jeunes</a></li>
		                            <li><a href="/parisdexil/pages/hebergeurs/hebergeurs.php">Hébergeurs</a></li>
		                            <li><a href="/parisdexil/pages/transferts/transferts.php">Transferts</a></li>
		                            <?php if ($_SESSION['admin']): ?>
			                            <li class="has-dropdown">
											<a href="#">Admin</a>
											<ul class="dropdown">
												<li><a href="../benevoles/benevoles.php">Bénévoles</a></li>
												<li><a href="../benevoles/personneInactif.php">Corbeille</a></li>
											</ul>
										</li>
									<?php endif;?>
		                            <li><a href="/parisdexil/pages/securite/deconnexion.php"><i class="fas fa-sign-out-alt"></i></a></li>
		                        </ul>
		                    </div>
		                </div>
		            </div>
		      	</nav>



