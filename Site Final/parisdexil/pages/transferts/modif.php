<?php $title="Transfert Modifié"; require_once("../../ressources/Model/Model.php");
	
	if ( isset($_POST['id_jeune'],$_POST['id_hebergeur'],$_POST['dateDebut'],$_POST['status']) )
	{
		$infos=[
				'id' => $_POST['id'],
				'num_hebergeur_id' => $_POST['id_hebergeur'],
				'num_mineur_id' => $_POST['id_jeune'],
				'date_debut' => $_POST['dateDebut'],
				'date_fin' => $_POST['dateFin'],
				'etat' => $_POST['status']	
		];

		$m->updateTransfert($infos);

		header('Location: ./transferts.php');
  		exit();
	}
	else
	{
		header('Location: ./modifTransfert.php?id='.$_POST['id']);
  		exit();
	}



?>