<?php $title="Jeunes Modifié"; require_once("../../ressources/Model/Model.php");
	
	if ( isset($_POST['nom'],
			   $_POST['prenom'],
			   $_POST['age'],
			   $_POST['genre'],
			   $_POST['autonomie'],
			   $_POST['dateArrivee'] )
		&& (isset($_POST['tel']) || isset($_POST['autrecontact']))
		)
	{
		// Information Personne
		$id_p=htmlspecialchars($_POST['id_p']);
		$nom=htmlspecialchars($_POST['nom']);
		$prenom=htmlspecialchars($_POST['prenom']);
		$tel=htmlspecialchars($_POST['tel']);
		$autrecontact=htmlspecialchars($_POST['autrecontact']);
		$description=htmlspecialchars($_POST['description']);
		$remarque=htmlspecialchars($_POST['remarque']);

		// Tags
		$autonomie=htmlspecialchars($_POST['autonomie']);
		$genre=htmlspecialchars($_POST['genre']);
		$tags= [$genre,$autonomie];
		


		$infos_personne=[
						  'id' => $id_p,
						  'nom' => $nom,
						  'prenom' => $prenom,
						  'telephone' => $tel,
						  'autre_contact' => $autrecontact,
						  'description' => $description,
						  'remarque' => $remarque,
						  'tags' => $tags,
						  'actif' => true
		];

		// Information Mineur
		$id=htmlspecialchars($_POST['id']);
		$age=htmlspecialchars($_POST['age']);
		$dateArrivee=htmlspecialchars($_POST['dateArrivee']);

		$infos_mineur=[
					   'id' => $id,
					   'age' => $age,
					   'heberge' => false,
					   'date_arrive' => $dateArrivee,
					   'genre'=>$genre
		];


		var_dump($id_p);
		var_dump($id);
		$m->updateMineur($infos_personne, $infos_mineur);

		header('Location: ./jeunes.php');
  		exit();
	}
	else
	{
		header('Location: ./modifJeune.php?id='.$_POST['id'].'&id_p='.$_POST['id_p']);
  		exit();
	}

?>