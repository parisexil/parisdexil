
<?php $title = "Corbeille";require_once "../header.php";
$benevoles = $m->getAllBenevole();
$hebergeurs = $m->getAllHebergeurs();
$jeunes = $m->getAllMineurs();?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/benevoles.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Personnes</span>
							<h1>Corbeille</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="gtco-features" class="border-bottom">
		<div class="gtco-container">

			<label>Les Bénévoles Inactifs</label>
			<hr>
			<div class="row">

				<?php $vide = 0;for ($i = 0; $i < sizeof($benevoles); $i++): ?>

					<?php if ($benevoles[$i]['actif'] == '0'): ?>
						<?php $vide++;?>
						<div class="col-md-3 col-sm-6">
							<div class="feature-center animate-box" data-animate-effect="fadeIn">
								<span class="icon">
									<img src="/parisdexil/ressources/images/photo-benevole.png" height="200" width="175"/>
								</span>
								<h3><?=$benevoles[$i]['nom'] . ' ' . $benevoles[$i]['prenom']?></h3>
								<p>
									<ul class="text-left">
										<li>Date d'ajout : <?=$benevoles[$i]['date_ajout']?></li>
									</ul>
								</p>
								<hr>
								<span class="badge badge-light"><?=$benevoles[$i]['admin'] == "1" ? "Admin" : " "?></span>
								<hr>
								<div class="form-row">
									<p class="col-md-7"><a href="./recup.php?id=<?=$benevoles[$i]['num_personne_id']?>" class="btn btn-default btn-block">Récupérer</a></p>
									<?php if ($benevoles[$i]['id'] != $_SESSION['id_b']): ?>
										<p class="col-md-5"><a href="./supp.php?id=<?=$benevoles[$i]['num_personne_id']?>" class="btn btn-danger btn-block"><i class="fas fa-trash"></i></a></p>
									<?php endif?>
								</div>
							</div>
						</div>

					<?php endif;?>

				<?php endfor;?>

				<?php if ($vide == 0): ?>

					<div class="col-md-4 col-md-offset-4 alert alert-info">
					  <strong>Il n'y a aucun bénévole inactif</strong>
					</div>

				<?php endif;?>

			</div>

			<label>Les Hébergeurs Inactifs</label>
			<hr>
			<div class="row">

				<?php $vide = 0;for ($i = 0; $i < sizeof($hebergeurs); $i++): ?>

					<?php if ($hebergeurs[$i]['actif'] == '0'): ?>
						<?php $vide++;?>
						<div class="col-md-3 col-sm-6">
							<div class="feature-center animate-box" data-animate-effect="fadeIn">
								<span class="icon">
									<img src="/parisdexil/ressources/images/photo-hebergeur.png" height="200" width="175"/>
								</span>
								<h3><?=$hebergeurs[$i]['nom'] . " " . $hebergeurs[$i]['prenom']?></h3>
								<hr>
								<?php foreach (unserialize(base64_decode($hebergeurs[$i]['tags'])) as $val): ?>
									<span class="badge badge-info"><?=$val?></span>
								<?php endforeach;?>
								<hr>
								<div class="form-row">
									<p class="col-md-7"><a href="./recup.php?id=<?=$hebergeurs[$i]['num_personne_id']?>" class="btn btn-default btn-block">Recupérer</a></p>
									<p class="col-md-5"><a href="./supp.php?id=<?=$hebergeurs[$i]['num_personne_id']?>" class="btn btn-danger btn-block"><i class="fas fa-trash"></i></a></p>
								</div>
							</div>
						</div>

					<?php endif;?>
				<?php endfor;?>
				<?php if ($vide == 0): ?>

					<div class="col-md-4 col-md-offset-4 alert alert-info">
					  <strong>Il n'y a aucun hébergeur inactif</strong>
					</div>

				<?php endif;?>
			</div>

			<label>Les Jeunes Inactifs</label>
			<hr>
			<div class="row">
				<?php $vide = 0;for ($i = 0; $i < sizeof($jeunes); $i++): ?>

					<?php if ($jeunes[$i]['actif'] == '0'): ?>
						<?php $vide++;?>
						<div class="col-md-3 col-sm-6">
							<div class="feature-center animate-box" data-animate-effect="fadeIn">
								<span class="icon">
									<img src="/parisdexil/ressources/images/photo-jeune.png" height="200" width="175"/>
								</span>
								<h3><?=$jeunes[$i]['nom'] . " " . $jeunes[$i]['prenom']?></h3>
								<p>
									<ul class="text-left">
										<li>Age : <?=$jeunes[$i]['age']?></li>
										<li>Date d'arrivée : <?=$jeunes[$i]['date_arrive']?></li>
									</ul>
								</p>
								<hr>

								<!-- ajout Tags-->
								<?php foreach (unserialize(base64_decode($jeunes[$i]['tags'])) as $val): ?>
									<?php if ($val == "Garçon"): ?>
										<span class="badge badge-info"><?=$val?></span>
									<?php endif;?>

									<?php if ($val == "Fille"): ?>
										<span class="badge badge-danger"><?=$val?></span>
									<?php endif;?>

									<?php if ($val == "Autonome"): ?>
										<span class="badge badge-success"><?=$val?></span>
									<?php endif;?>

									<?php if ($val == "Non Autonome"): ?>
										<span class="badge badge-warning"><?=$val?></span>
									<?php endif;?>
								<?php endforeach;?>
								<hr>
								<div class="form-row">
									<p class="col-md-7"><a href="./recup.php?id=<?=$jeunes[$i]['num_personne_id']?>" class="btn btn-default btn-block">Récupérer</a></p>
									<p class="col-md-5"><a href="./supp.php?id=<?=$jeunes[$i]['num_personne_id']?>" class="btn btn-danger btn-block"><i class="fas fa-trash"></i></a></p>
								</div>
							</div>
						</div>

					<?php endif;?>
				<?php endfor;?>
				<?php if ($vide == 0): ?>

					<div class="col-md-4 col-md-offset-4 alert alert-info">
					  <strong>Il n'y a aucun jeune inactif</strong>
					</div>

				<?php endif;?>

			</div>
		</div>
	</div>

<?php require_once '../footer.html'?>