
<?php $title="Modifier un Hébergeur"; require_once("../header.php") ;?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/img_4.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small"></span>
							<h1>Modifier un Hébergeur</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	
	<div class="gtco-section">
		<div>
			<form class="form" action="./modif.php" method="post">
				<label>Informations Personnelles</label>
				<hr>
				<div class="form-group">
					<br>
					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="nom">Nom <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="nom" value="<?= $nom ?>">
						</div>

						<div class="form-group col-md-5">
							<label for="prenom">Prénom <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="prenom" value="<?= $prenom ?>">
						</div>
						<div class="form-group col-md-2">
							<label for="Genre">Genre <span class="obligatoire">*</span></label>
							<select id="Genre" class="form-control">
								<option>Choisir ...</option>
								<option <?= $genre=="Masculin" ? "selected" : "" ?>>Masculin</option>
								<option <?= $genre=="Feminin" ? "selected" : "" ?>>Féminin</option>
							</select>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="tel">Téléphone <span class="obligatoire">*</span></label>
							<input type="tel" class="form-control" id="tel" value="<?= $tel ?>">
						</div>

						<div class="form-group col-md-8">
							<label for="autrecontact">Autre Contact (si pas de téléphone) <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="autrecontact" value="<?= $autreContact ?>">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="description">Description</label>
							<textarea id="description" class="form-control" name="description" rows="5" value="<?= $description ?>"></textarea>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="description">Description Endroit</label>
							<textarea id="description" class="form-control" name="descriptionEndroit" rows="5" value="<?= $descriptionEndroit ?>" ></textarea>
						</div>
					</div>

					<br>
					<div class="form-row">
						<div class="form-group col-md-3">
							<label>Donne la clé ou pas <span class="obligatoire">*</span></label>
							<hr>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="cle" id="cle" value="Donne la clé" <?= $donneCle=="oui" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="cle">
							    	Donne la clé
							  	</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="cle" id="noncle" value="Ne donne pas la clé" <?= $donneCle=="non" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="noncle">
							    	Ne donne pas la clé
							  	</label>
							</div>
						</div>

						<div class="form-group col-md-1"></div>

						<div class="form-group col-md-4">
							<label>Acceuille qui ? <span class="obligatoire">*</span></label>
							<hr>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="accueil" id="fille" value="Fille" <?= $accueil=="Fille" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="fille">
							    	Accueille que des filles
							  	</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="accueil" id="garçon" value="Garçon" <?= $accueil=="Garçon" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="garçon">
							    	Accueille que des garçons
							  	</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="accueil" id="jeunes" value="Jeunes Autonomes" <?= $accueil=="Jeunes Autonomes" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="jeunes">
							    	Accueille que les jeunes autonomes
							  	</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="accueil" id="pasdepreference" value="Pas de préférence" <?= $accueil=="Pas de préférence" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="pasdepreference">
							    	Pas de préférence
							  	</label>
							</div>
						</div>

						<div class="form-group col-md-1"></div>

						<div class="form-group col-md-3">
							<label>Hébergement <span class="obligatoire">*</span></label>
							<hr>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="habitat" id="foyer" value="Héberge dans le foyer" <?= $foyer=="Héberge dans le foyer" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="foyer">
							    	Héberge dans le foyer
							  	</label>
							</div>
							<div class="form-check">
							  	<input class="form-check-input position-static" type="radio" name="habitat" id="pret" value="Prêt d'appartement" <?= $foyer=="Prêt d'appartement" ? "checked" : "" ?>>
							  	<label class="form-check-label" for="pret">
							    	Prêt d'appartement
							  	</label>
							</div>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="datearrivee">Date d'arrivée <span class="obligatoire">*</span></label>
							<input type="date" class="form-control" id="datearrivee" value="<?= $dateArrivee ?>">
						</div>
					</div>
					<br>
					<label>Disponibilités</label>
					<hr>
					<div class="form-group" id="periode">
						<div class="form-row">
							<div class="form-group col-md-2">
								<div class="btn" id="ajoutPeriode">Ajouter une période</div>
							</div>
						</div>
						<?php for($i=0; $i<sizeof($disponibilite); $i++): ?>
							<div class="form-row">
								<div class="form-group col-md-5">
									<label for="datededebut">Date début</label>
									<input type="date" class="form-control" name="datededebut" id="datededebut" value="<?php $disponibilite[$i]['dateDebut'] ?>">
								</div>

								<div class="form-group col-md-5">
									<label for="datedefin">Date de fin</label>
									<input type="date" class="form-control" name="datedefin" id="datedefin" value="<?php $disponibilite[$i]['dateFin'] ?>">
								</div>
								<div class="form-group col-md-1">
									<br>
									<i class="fas fa-times fa-2x" id="suppPer"></i>
								</div>
							</div>
						<?php endfor; ?>
					</div>
				</div>
				<br>

				<label>Informations Supplémentaires</label>
				<hr>
				<div class="form-group">
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="remarque">Remarques</label>
							<textarea id="remarque" class="form-control" name="remarque" rows="5" value="<?= $remarque ?>" ></textarea>
						</div>
					</div>
			  	</div>
			  	<br>
			  	<div class="form-row">
					<button type="submit" class="btn btn-primary">Modifier</button>
				</div>
			</form>
		</div>
	</div>

<?php require_once("../footer.html") ;?>