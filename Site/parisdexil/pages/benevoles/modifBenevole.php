
<?php $title="Modifier un Bénévole"; require_once("../header.php") ;?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/img_4.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small"></span>
							<h1>Modifier un bénévole</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	
	<div class="gtco-section">
		<div>
			<form class="form" action="./ajout.php" method="post">
				<label>Informations Personnelles</label>
				<hr>
				<div class="form-group">
					<br>
					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="nom">Nom <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="nom" value="<?= $nom ?>">
						</div>

						<div class="form-group col-md-5">
							<label for="prenom">Prénom <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="prenom" value="<?= $prenom ?>">
						</div>
						<div class="form-group col-md-2">
							<label for="Genre">Genre <span class="obligatoire">*</span></label>
							<select id="Genre" class="form-control">
								<option>Choisir ...</option>
								<option <?= $genre=="Masculin" ? "selected" : "" ?>>Masculin</option>
								<option <?= $genre=="Feminin" ? "selected" : "" ?>>Féminin</option>
							</select>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-4">
							<label for="tel">Téléphone <span class="obligatoire">*</span></label>
							<input type="tel" class="form-control" id="tel" value="<?= $tel ?>">
						</div>

						<div class="form-group col-md-8">
							<label for="autrecontact">Autre Contact (si pas de téléphone) <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="autrecontact" value="<?= $autreContact ?>">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="description">Description</label>
							<textarea id="description" class="form-control" name="description" rows="5" value="<?= $description ?>"></textarea>
						</div>
					</div>

					<br>

					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="datearrivee">Date d'arrivée <span class="obligatoire">*</span></label>
							<input type="date" class="form-control" id="datearrivee" value="<?= $dateArrivee ?>">
						</div>
					</div>
					<br>
				</div>
				<br>

				<label>Informations Supplémentaires</label>
				<hr>
				<div class="form-group">
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="remarque">Remarques</label>
							<textarea id="remarque" class="form-control" name="remarque" rows="5" value="<?= $remarque ?>" ></textarea>
						</div>
					</div>
			  	</div>
			  	<br>
			  	<div class="form-row">
					<button type="submit" class="btn btn-primary">Modifier</button>
				</div>
			</form>
		</div>
	</div>

<?php require_once("../footer.html") ;?>