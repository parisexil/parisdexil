<?php
class Model {

	/**
	 * l'instance PDO
	 */
	private $db;

	/*
		    *   unique instance de Model (singleton)
	*/
	private static $instance = null;

	public const ETAT_TRANSFERT_1 = "aVenir";
	public const ETAT_TRANSFERT_2 = "enCours";
	public const ETAT_TRANSFERT_3 = "terminé";
	public const ETAT_TRANSFERT_4 = "annulé";

	/**
	 * constructeur privé
	 */
	private function __construct() {

		/**
		 *
		 * NE PAS OUBLIER DE CREER LA BDD AVEC LE SCRIPT SQL
		 */

		try {
			$dsn = "mysql:host=localhost;dbname=parisdexil"; //A modifier en fonction de votre base
			$login = "admin";
			$password = "admin";
			$this->db = new PDO($dsn, $login, $password);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db->query("SET names 'utf8'");
		} catch (PDOException $e) {
			die('Echec connexion, erreur n°' . $e->getCode() . ':' . $e->getMessage());
		}
	}

	/**
	 * Renvoie l'instance unique de Model
	 *
	 * @return <Model>
	 */
	public static function getModel() {

		if (is_null(self::$instance)) {
			self::$instance = new Model();
		}

		return self::$instance;
	}

	/*
		   	 +----------------------------------+
		   	 *                                  *
		   	 *            PERSONNE              *
		   	 *                                  *
		   	 +----------------------------------+
	*/

	/**
	 * Permet d'ajouter une personne
	 * @param <array> $infos contient toutes les infos de la personne,
	 * 	                  nom et prenom sont obligatoires
	 */
	public function addPersonne($infos) {
		try {
			extract($infos);

			if (isset($nom, $prenom)) {
				if (!isset($actif)) {
					$actif = true;
				}

				if (isset($tags)) {
					$tags = base64_encode(serialize($tags));
				}

				$req = $this->db->prepare('INSERT INTO `personne` VALUES (NULL, current_timestamp, :nom, :prenom, :telephone, :autre_contact, :description, :remarque, :tags, :actif);');

				$req->bindValue(':nom', $nom);
				$req->bindValue(':prenom', $prenom);
				$req->bindValue(':telephone', $telephone);
				$req->bindValue(':autre_contact', $autre_contact);
				$req->bindValue(':description', $description);
				$req->bindValue(':remarque', $remarque);
				$req->bindValue(':tags', $tags);
				$req->bindValue(':actif', $actif);

				$result = $req->execute();

				if ($result === false) {return false;}

				return $this->getModel()->getLastId_Personne();

			} else {
				return false;
			}
		} catch (PDOException $e) {
			return false;
		}
	}

	public function deletePersonne($id) {
		try {

			/*Recuperation des id de mineurs et hebergeurs si existe*/
			$req_m = $this->db->prepare('SELECT id FROM `mineur` as m WHERE m.num_personne_id = :id');
			$req_m->bindValue(':id', $id);
			$req_m->execute();

			$req_h = $this->db->prepare('SELECT id FROM `hebergeur` as h WHERE h.num_personne_id = :id');
			$req_h->bindValue(':id', $id);
			$req_h->execute();

			$id_m = (int) $req_m->fetch(PDO::FETCH_ASSOC);
			$id_h = (int) $req_h->fetch(PDO::FETCH_ASSOC);

			// $id_h = ($id_h == null) ? 0 : $id_h;
			// $id_m = ($id_m == null) ? 0 : $id_m;

			/*Suppression de toutes les occurences de $id*/
			$req_b = $this->db->prepare('DELETE FROM `benevole` WHERE num_personne_id = :id ');
			$req_b->bindValue(':id', $id);
			$req_b->execute();

			$req_h = $this->db->prepare('DELETE FROM `hebergeur` WHERE num_personne_id = :id ');
			$req_h->bindValue(':id', $id);
			$req_h->execute();

			$req_m = $this->db->prepare('DELETE FROM `mineur` WHERE num_personne_id = :id ');
			$req_m->bindValue(':id', $id);
			$req_m->execute();

			$req_p = $this->db->prepare('DELETE FROM `personne` WHERE id = :id ');
			$req_p->bindValue(':id', $id);
			$req_p->execute();

			/*Suppression des transferts contenant les id_m ou id_h */

			$req_t = $this->db->prepare('DELETE FROM `transfert` WHERE num_mineur_id = :id_m OR num_hebergeur_id = :id_h');

			$req_t->bindValue(':id_m', $id_m);
			$req_t->bindValue(':id_h', $id_h);

			$req_t->execute();

			return [$req_t, $req_m, $req_p, $req_h, $req_b];

		} catch (PDOException $e) {
			echo $e->getMessage();
			return false;
		}
	}

	public function disablePersonne($id) {
		try {

			$req = $this->db->prepare('UPDATE `personne` SET `actif` = 0 WHERE id = :id');

			$req->bindValue(':id', $id);
			$result = $req->execute();

			return $result;

		} catch (PDOException $e) {
			return false;
		}
	}

	public function enabledPersonne($id) {
		try {

			$req = $this->db->prepare('UPDATE `personne` SET `actif` = 1 WHERE id = :id');

			$req->bindValue(':id', $id);
			$result = $req->execute();

			return $result;

		} catch (PDOException $e) {
			return false;
		}
	}

	public function updatePersonne($infos_personne) {

		try {
			extract($infos_personne);

			if (isset($id, $nom, $prenom)) {

				if (isset($tags)) {
					$tags = base64_encode(serialize($tags));
				}

				$req = $this->db->prepare('UPDATE `personne` SET `nom`= :nom,`prenom`= :prenom,`telephone`= :telephone,`autre_contact`= :autre_contact,`description`= :description,`remarque`= :remarque,`tags`= :tags,`actif`= :actif WHERE id = :id');

				$req->bindValue(':id', $id);
				$req->bindValue(':nom', $nom);
				$req->bindValue(':prenom', $prenom);
				$req->bindValue(':telephone', $telephone);
				$req->bindValue(':autre_contact', $autre_contact);
				$req->bindValue(':description', $description);
				$req->bindValue(':remarque', $remarque);
				$req->bindValue(':tags', $tags);
				$req->bindValue(':actif', $actif);

				$result = $req->execute();

				if ($result === false) {return false;}

			} else {
				return false;
			}

		} catch (PDOException $e) {
			return false;
		}
	}

	/**
	 * Renvoie le nombre de Personne total
	 * @return int nombre de personne
	 */
	public function getNbPersonne() {
		$req = $this->db->prepare('SELECT count(*) FROM `personne` WHERE actif = 1');
		$req->execute();

		$nb = $req->fetch(PDO::FETCH_ASSOC);

		return (int) $nb['count(*)'];
	}

	/**
	 * Renvoie le derniere id
	 * @return id de la derniere personne
	 */
	public function getLastId_Personne() {
		$req = $this->db->prepare('SELECT id FROM `personne` ORDER BY id DESC LIMIT 1 ');
		$req->execute();

		$nb = $req->fetch(PDO::FETCH_ASSOC);

		return (int) $nb['id'];
	}

	/*
		   	 +----------------------------------+
		   	 *                                  *
		   	 *            BENEVOLE              *
		   	 *                                  *
		   	 +----------------------------------+
	*/

	/**
	 * Permet d'ajouter un benevole
	 * @param <array> $infos_personne contient toutes les infos de la personne,
	 * 	                  nom et prenom sont obligatoires
	 *
	 * @param <array> $infos_benevole contient toutes les infos du benevole,
	 * 	                  nom et prenom sont obligatoires
	 */
	public function addBenevole($infos_personne, $infos_benevole) {
		$infos_benevole['num_personne_id'] = $this->getModel()->addPersonne($infos_personne);

		try {
			extract($infos_benevole);

			if (isset($mail, $identifiant, $mdp)) {
				if (!isset($admin)) {
					$admin = false;
				}

				$req = $this->db->prepare('INSERT INTO `benevole` VALUES (NULL, :num_personne_id, current_timestamp, :mail, :identifiant, :mdp, :admin);');

				$req->bindValue(':num_personne_id', $num_personne_id);
				$req->bindValue(':mail', $mail);
				$req->bindValue(':identifiant', $identifiant);
				$req->bindValue(':mdp', $mdp);
				$req->bindValue(':admin', $admin ? 1 : 0);

				$result = $req->execute();

				if ($result === false) {return false;}

				return $this->getModel()->getLastId_Benevole();

			} else {
				return false;
			}
		} catch (PDOException $e) {
			return false;
		}
	}

	public function updateBenevole($infos_personne, $infos_benevole) {
		$this->getModel()->updatePersonne($infos_personne);

		try {
			extract($infos_benevole);

			if (isset($id, $mail, $identifiant, $mdp, $admin)) {

				$req = $this->db->prepare('UPDATE `benevole` SET `mail`= :mail,`identifiant`= :identifiant,`mdp`= :mdp,`admin`= :admin WHERE id = :id');

				$req->bindValue(':id', $id);
				$req->bindValue(':mail', $mail);
				$req->bindValue(':identifiant', $identifiant);
				$req->bindValue(':mdp', $mdp);
				$req->bindValue(':admin', $admin);

				$result = $req->execute();

				if ($result === false) {return false;}

			} else {
				return false;
			}

		} catch (PDOException $e) {
			return false;
		}
	}

	public function login($username, $password) {

		try {

			$valide = false;
			$req = $this->db->prepare('SELECT id, mdp FROM `benevole` WHERE identifiant = :identifiant ');

			$req->bindValue(':identifiant', $username);

			$result = $req->execute();
			if ($result === false) {return false;}

			$tab = $req->fetch(PDO::FETCH_ASSOC);
			$valide = password_verify($password, $tab['mdp']);

			return ['id_b' => $tab['id'], 'valide' => $valide];

		} catch (PDOException $e) {
			return false;
		}

	}

	public function getAllBenevole() {

		try {
			$requete = $this->db->prepare('SELECT  *  from `personne` as p join `benevole` as b WHERE p.id = b.num_personne_id ORDER BY b.date_ajout DESC');
			$requete->execute();
			return $requete->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			return false;
		}
	}

	public function getBenevoleById($id) {
		if ((string) $id !== (string) (int) $id) {return false;}

		try {

			$req = $this->db->prepare('SELECT p.nom, p.prenom, p.telephone, p.autre_contact, p.description, p.remarque, p.tags, p.actif ,b.*  FROM `benevole` as b join `personne` as p WHERE b.num_personne_id = p.id AND b.id = :id');

			$req->bindValue(':id', (int) $id);

			$result = $req->execute();

			if ($result === false) {return false;}

			return $req->fetch(PDO::FETCH_ASSOC);

		} catch (PDOException $e) {
			return false;
		}
	}

	/**
	 * Renvoie le nombre de Benevole total
	 * @return int nombre de Benevole
	 */
	public function getNbBenevole() {
		$req = $this->db->prepare('SELECT count(*) FROM `benevole` as b , `personne` as p WHERE p.id = b.num_personne_id AND p.actif = 1');
		$req->execute();

		$nb = $req->fetch(PDO::FETCH_ASSOC);

		return (int) $nb['count(*)'];
	}

	/**
	 * Renvoie le derniere id
	 * @return int id du dernier benevole
	 */
	public function getLastId_Benevole() {
		$req = $this->db->prepare('SELECT id FROM `benevole` ORDER BY id DESC LIMIT 1 ');
		$req->execute();

		$nb = $req->fetch(PDO::FETCH_ASSOC);

		return (int) $nb['id'];
	}

	/*
		   	 +----------------------------------+
		   	 *                                  *
		   	 *            MINEUR                *
		   	 *                                  *
		   	 +----------------------------------+
	*/

	public function addMineur($infos_personne, $infos_mineur) {
		$infos_mineur['num_personne_id'] = $this->getModel()->addPersonne($infos_personne);

		try {
			extract($infos_mineur);

			if (isset($age, $genre)) {
				if (!isset($heberge)) {$heberge = false;}

				$req = $this->db->prepare('INSERT INTO `mineur` VALUES (NULL, :num_personne_id, current_timestamp, :age, :heberge, :date_arrive, :genre);');

				$req->bindValue(':num_personne_id', $num_personne_id);
				$req->bindValue(':age', $age);
				$req->bindValue(':heberge', ($heberge) ? 1 : 0);
				$req->bindValue(':date_arrive', $date_arrive);
				$req->bindValue(':genre', ($genre) ? 1 : 0); //True = M , False = F

				$result = $req->execute();

				if ($result === false) {return false;}

				return $this->getModel()->getLastId_Mineur();

			} else {
				return false;
			}
		} catch (PDOException $e) {
			return false;
		}
	}

	public function updateMineur($infos_personne, $infos_mineur) {
		$this->getModel()->updatePersonne($infos_personne);

		try {
			extract($infos_mineur);

			if (isset($id, $age, $heberge, $date_arrive, $genre)) {

				$req = $this->db->prepare('UPDATE `mineur` SET `age`=:age,`heberge`=:heberge,`date_arrive`=:date_arrive,`genre`=:genre WHERE id = :id');

				$req->bindValue(':id', $id);
				$req->bindValue(':age', $age);
				$req->bindValue(':heberge', $heberge);
				$req->bindValue(':date_arrive', $date_arrive);
				$req->bindValue(':genre', $genre);

				$result = $req->execute();

				if ($result === false) {return false;}

			} else {
				return false;
			}

		} catch (PDOException $e) {
			return false;
		}
	}

	public function getMineurById($id) {
		if ((string) $id !== (string) (int) $id) {return false;}

		try {

			$req = $this->db->prepare('SELECT p.nom, p.prenom, p.telephone, p.autre_contact, p.description, p.remarque, p.tags, p.actif ,m.* FROM `mineur` as m join `personne` as p WHERE m.num_personne_id = p.id AND m.id = :id');

			$req->bindValue(':id', (int) $id);

			$result = $req->execute();

			if ($result === false) {return false;}

			return $req->fetch(PDO::FETCH_ASSOC);

		} catch (PDOException $e) {
			return false;
		}
	}

	public function getAllMineurs() {

		try {
			$requete = $this->db->prepare('SELECT  *  from `personne` as p join `mineur` as m WHERE p.id = m.num_personne_id ORDER BY m.date_ajout DESC');
			$requete->execute();
			return $requete->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			return false;
		}
	}

	/**
	 * Renvoie le nombre de Mineur total
	 * @return int nombre de Mineur
	 */
	public function getNbMineur() {
		$req = $this->db->prepare('SELECT count(*) FROM `mineur` as m , `personne` as p WHERE p.id = m.num_personne_id AND p.actif = 1');
		$req->execute();

		$nb = $req->fetch(PDO::FETCH_ASSOC);

		return (int) $nb['count(*)'];
	}
	/**
	 * Renvoie le derniere id
	 * @return int id du dernier mineur
	 */
	public function getLastId_Mineur() {
		$req = $this->db->prepare('SELECT id FROM `mineur` ORDER BY id DESC LIMIT 1 ');
		$req->execute();

		$nb = $req->fetch(PDO::FETCH_ASSOC);

		return (int) $nb['id'];
	}

	/*
		   	 +----------------------------------+
		   	 *                                  *
		   	 *            HEBERGEUR             *
		   	 *                                  *
		   	 +----------------------------------+
	*/

	/**
	 * Permet d'ajouter un hébergeur
	 * @param <array> $infos_personne contient toutes les infos de la personne
	 *
	 * @param <array> $infos_hebergeur contient toutes les infos de l'hébergeur
	 */
	public function addHebergeur($infos_personne, $infos_hebergeur) {
		$infos_hebergeur['num_personne_id'] = $this->getModel()->addPersonne($infos_personne);

		try {
			extract($infos_hebergeur);

			if (isset($adresse, $code_postal, $ville)) {

				if (isset($disponibilite)) {
					$disponibilite = base64_encode(serialize($disponibilite));
				}

				$req = $this->db->prepare('INSERT INTO `hebergeur` VALUES (NULL, :num_personne_id, current_timestamp, :adresse, :code_postal, :ville, :disponibilite, :description_endroit);');

				$req->bindValue(':num_personne_id', $num_personne_id);
				$req->bindValue(':adresse', $adresse);
				$req->bindValue(':code_postal', $code_postal);
				$req->bindValue(':ville', $ville);
				$req->bindValue(':disponibilite', $disponibilite);
				$req->bindValue(':description_endroit', $description_endroit);

				$result = $req->execute();

				if ($result === false) {return false;}

				return $this->getModel()->getLastId_Hebergeur();

			} else {
				return false;
			}

		} catch (PDOException $e) {
			return false;
		}
	}

	public function updateHebergeur($infos_personne, $infos_hebergeur) {
		$this->getModel()->updatePersonne($infos_personne);

		try {
			extract($infos_hebergeur);

			if (isset($id, $adresse, $code_postal, $ville)) {

				if (isset($disponibilite)) {
					$disponibilite = base64_encode(serialize($disponibilite));
				}

				$req = $this->db->prepare('UPDATE `hebergeur` SET `adresse`=:adresse ,`code_postal`=:code_postal,`ville`=:ville,`disponibilite`=:disponibilite ,`description_endroit`= :description_endroit WHERE id = :id');

				$req->bindValue(':id', $id);
				$req->bindValue(':adresse', $adresse);
				$req->bindValue(':code_postal', $code_postal);
				$req->bindValue(':ville', $ville);
				$req->bindValue(':disponibilite', $disponibilite);
				$req->bindValue(':description_endroit', $description_endroit);

				$result = $req->execute();

				if ($result === false) {return false;}

			} else {
				return false;
			}

		} catch (PDOException $e) {
			return false;
		}
	}

	public function getHebergeurById($id) {
		if ((string) $id !== (string) (int) $id) {return false;}

		try {

			$req = $this->db->prepare('SELECT p.nom, p.prenom, p.telephone, p.autre_contact, p.description, p.remarque, p.tags, p.actif ,h.* FROM `hebergeur` as h join `personne` as p WHERE h.num_personne_id = p.id AND h.id = :id');

			$req->bindValue(':id', (int) $id);

			$result = $req->execute();

			if ($result === false) {return false;}

			return $req->fetch(PDO::FETCH_ASSOC);

		} catch (PDOException $e) {
			return false;
		}
	}

	public function getAllHebergeurs() {

		try {
			$requete = $this->db->prepare('SELECT * from `personne` as p join `hebergeur` as h WHERE p.id = h.num_personne_id ORDER BY h.date_ajout DESC');
			$requete->execute();
			return $requete->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			return false;
		}
	}

	/**
	 * Renvoie le nombre de Hebergeur total
	 * @return int nombre de Hebergeur
	 */
	public function getNbHebergeur() {
		$req = $this->db->prepare('SELECT count(*) FROM `hebergeur` as h , `personne` as p WHERE p.id = h.num_personne_id AND p.actif = 1');
		$req->execute();

		$nb = $req->fetch(PDO::FETCH_ASSOC);

		return (int) $nb['count(*)'];
	}

	/**
	 * Renvoie le derniere id
	 * @return int id du dernier hebergeur
	 */
	public function getLastId_Hebergeur() {
		$req = $this->db->prepare('SELECT id FROM `hebergeur` ORDER BY id DESC LIMIT 1 ');
		$req->execute();

		$nb = $req->fetch(PDO::FETCH_ASSOC);

		return (int) $nb['id'];
	}

	/*
		   	 +----------------------------------+
		   	 *                                  *
		   	 *     TRANSFERTS | avec etats      *
		   	 *                                  *
		   	 +----------------------------------+
	*/

	/**
	 * Permet d'ajouter une personne
	 * @param <array> $infos contient toutes les infos du transfert
	 *
	 */
	public function addTransfert($infos) {

		try {
			extract($infos);

			if (isset($num_hebergeur_id, $num_mineur_id, $date_debut, $date_fin, $etat)) {

				$req = $this->db->prepare('INSERT INTO `transfert`(`id`, `num_hebergeur_id`, `num_mineur_id`, `date_ajout`, `date_debut`, `date_fin`, `etat`) VALUES (NULL,:num_hebergeur_id,:num_mineur_id,CURRENT_TIMESTAMP,:date_debut,:date_fin,:etat)');

				$req->bindValue(':num_hebergeur_id', $num_hebergeur_id);
				$req->bindValue(':num_mineur_id', $num_mineur_id);
				$req->bindValue(':date_debut', $date_debut);
				$req->bindValue(':date_fin', $date_fin);
				$req->bindValue(':etat', $etat);

				$result = $req->execute();

				if ($result === false) {return false;}

				return $this->getModel()->getLastId_Transfert();

			} else {
				return false;
			}
		} catch (PDOException $e) {
			echo $e->getMessage();
			return false;
		}
	}

	public function updateTransfert($infos) {

		try {
			extract($infos);

			if (isset($id, $num_hebergeur_id, $num_mineur_id, $date_debut, $date_fin, $etat)) {

				$req = $this->db->prepare('UPDATE `transfert` SET  `num_hebergeur_id`= :num_hebergeur_id ,`num_mineur_id`= :num_mineur_id,`date_debut`= :date_debut,`date_fin`= :date_fin,`etat`= :etat WHERE id = :id');

				$req->bindValue(':id', $id);
				$req->bindValue(':num_hebergeur_id', $num_hebergeur_id);
				$req->bindValue(':num_mineur_id', $num_mineur_id);
				$req->bindValue(':date_debut', $date_debut);
				$req->bindValue(':date_fin', $date_fin);
				$req->bindValue(':etat', $etat);

				$result = $req->execute();

				if ($result === false) {return false;}

			} else {
				return false;
			}

		} catch (PDOException $e) {
			return false;
		}
	}

	public function deleteTransfert($id_transfert) {

		try {

			if ((string) $id_transfert !== (string) (int) $id_transfert) {return false;}

			$req = $this->db->prepare('DELETE FROM `transfert` WHERE id = :id');

			$req->bindValue(':id', $id_transfert);

			$result = $req->execute();

			return $result;

		} catch (PDOException $e) {
			return false;
		}
	}

	public function getNbTransfertForHebergeur($id_hebergeur) {
		if ((string) $id_hebergeur !== (string) (int) $id_hebergeur) {return false;}

		try {

			$req = $this->db->prepare('SELECT count(*) FROM `transfert` as t WHERE  t.num_hebergeur_id = :id');

			$req->bindValue(':id', (int) $id_hebergeur);

			$result = $req->execute();

			if ($result === false) {return false;}

			$tab = $req->fetch(PDO::FETCH_ASSOC);

			return (int) $tab['count(*)'];

		} catch (PDOException $e) {
			return false;
		}
	}

	public function getNbTransfertForMineur($id_mineur) {
		if ((string) $id_mineur !== (string) (int) $id_mineur) {return false;}

		try {

			$req = $this->db->prepare('SELECT count(*) FROM `transfert` as t WHERE  t.num_mineur_id = :id');

			$req->bindValue(':id', (int) $id_mineur);

			$result = $req->execute();

			if ($result === false) {return false;}

			$tab = $req->fetch(PDO::FETCH_ASSOC);

			return (int) $tab['count(*)'];

		} catch (PDOException $e) {
			return false;
		}
	}

	public function getTransfertById($id) {
		if ((string) $id !== (string) (int) $id) {return false;}

		try {

			$req = $this->db->prepare('SELECT
					pp.nom nom_m, pp.prenom prenom_m, t.date_debut, t.date_fin, t.id, t.etat ,p.nom nom_h, p.prenom prenom_h
	         FROM
				    `personne` AS pp
				JOIN `mineur` AS m
				JOIN `transfert` AS t
				JOIN `hebergeur` AS h
				JOIN `personne` AS p
				WHERE
					p.id = h.num_personne_id AND h.id = t.num_hebergeur_id AND t.num_mineur_id = m.id AND m.num_personne_id = pp.id AND t.id = :id');

			$req->bindValue(':id', (int) $id);

			$result = $req->execute();

			if ($result === false) {return false;}

			return $req->fetch(PDO::FETCH_ASSOC);

		} catch (PDOException $e) {
			return false;
		}
	}

	public function getAllTransfert() {
		try {
			$req = $this->db->prepare('SELECT
     											pp.nom nom_m, pp.prenom prenom_m, t.date_debut, t.date_fin, t.id, t.etat ,p.nom nom_h, p.prenom prenom_h
  				FROM
  				    `personne` AS pp
  				JOIN `mineur` AS m
  				JOIN `transfert` AS t
  				JOIN `hebergeur` AS h
  				JOIN `personne` AS p
  				WHERE
  					p.id = h.num_personne_id AND h.id = t.num_hebergeur_id AND t.num_mineur_id = m.id AND m.num_personne_id = pp.id

  					ORDER BY t.date_ajout DESC');

			$result = $req->execute();

			if ($result === false) {return false;}

			return $req->fetchAll(PDO::FETCH_ASSOC);

		} catch (PDOException $e) {
			return false;
		}
	}

	public function getAllTransfertForHebergeur($id_hebergeur) {
		try {

			if ((string) $id_hebergeur !== (string) (int) $id_hebergeur) {return false;}

			$req = $this->db->prepare('SELECT t.id FROM `transfert` as t WHERE t.num_hebergeur_id = :id ORDER BY t.date_ajout DESC');

			$req->bindValue(':id', (int) $id_hebergeur);
			$result = $req->execute();

			if ($result === false) {return false;}

			$tab_id = $req->fetchAll(PDO::FETCH_ASSOC);

			$tab_result = [];

			foreach ($tab_id as $v) {
				$tab_result[] = $this->getTransfertById($v['id']);
			}

			return $tab_result;

		} catch (PDOException $e) {
			return false;
		}
	}

	public function getAllTransfertForMineur($id_mineur) {
		try {

			if ((string) $id_mineur !== (string) (int) $id_mineur) {return false;}

			$req = $this->db->prepare('SELECT t.id FROM `transfert` as t WHERE t.num_mineur_id = :id ORDER BY t.date_ajout DESC');

			$req->bindValue(':id', (int) $id_mineur);
			$result = $req->execute();

			if ($result === false) {return false;}

			$tab_id = $req->fetchAll(PDO::FETCH_ASSOC);

			$tab_result = [];

			foreach ($tab_id as $v) {
				$tab_result[] = $this->getTransfertById($v['id']);
			}

			return $tab_result;

		} catch (PDOException $e) {
			return false;
		}
	}

	/**
	 * Renvoie le nombre de Transfert total
	 * @return int nombre de Transfert
	 */
	public function getNbTransfert() {
		$req = $this->db->prepare('SELECT count(*) FROM `transfert`');
		$req->execute();

		$nb = $req->fetch(PDO::FETCH_ASSOC);

		return (int) $nb['count(*)'];
	}

	/**
	 * Renvoie le derniere id
	 * @return int id du dernier transfert
	 */
	public function getLastId_Transfert() {
		$req = $this->db->prepare('SELECT id FROM `transfert` ORDER BY id DESC LIMIT 1 ');
		$req->execute();

		$nb = $req->fetch(PDO::FETCH_ASSOC);

		return (int) $nb['id'];
	}

}

$m = Model::getModel();
