<?php $title="Hébergeur Ajouté"; require_once("../../ressources/Model/Model.php");
	
	if ( isset($_POST['nom'],
			   $_POST['prenom'],
			   $_POST['genre'],
			   $_POST['adresse'],
			   $_POST['ville'],
			   $_POST['codepostal'] )
		&& (isset($_POST['tel']) || isset($_POST['autrecontact']))
		)
	{
		// Information Personne
		$nom=htmlspecialchars($_POST['nom']);
		$prenom=htmlspecialchars($_POST['prenom']);
		$genre=htmlspecialchars($_POST['genre']);
		$tel=htmlspecialchars($_POST['tel']);
		$autrecontact=htmlspecialchars($_POST['autrecontact']);
		$description=htmlspecialchars($_POST['description']);
		$descriptionEndroit=htmlspecialchars($_POST['descriptionEndroit']);
		$remarque=htmlspecialchars($_POST['remarque']);
		$cle=htmlspecialchars($_POST['cle']);
		$accueil=htmlspecialchars($_POST['accueilqui']);
		$habitat=htmlspecialchars($_POST['habitat']);
		$tags=[$cle,$accueil,$habitat];


		$infos_personne=[
						  'nom' => $nom,
						  'prenom' => $prenom,
						  'telephone' => $tel,
						  'autre_contact' => $autrecontact,
						  'description' => $description,
						  'remarque' => $remarque,
						  'tags' => $tags,
						  'actif' => true
		];

		// Information hebergeurs
		$adresse=htmlspecialchars($_POST['adresse']);
		$ville=htmlspecialchars($_POST['ville']);
		$codepostal=htmlspecialchars($_POST['codepostal']);
		$disponibilite=[];

		for($i=0; $i<10; $i++)
		{
			if (isset($_POST['datededebut'.$i],$_POST['datedefin'.$i]))
			{
				array_push($disponibilite, [$_POST['datededebut'.$i],$_POST['datedefin'.$i]]);
			}
		}

		$infos_hebergeur=[
						   'adresse' => $adresse,
						   'disponibilite' => $disponibilite,
						   'code_postal' => $codepostal,
						   'ville' => $ville,
						   'description_endroit' => $descriptionEndroit
		];

		$m->addHebergeur($infos_personne, $infos_hebergeur);

		header('Location: ./hebergeurs.php');
  		exit();
	}
	else
	{
		header('Location: ./ajoutHebergeur.php');
  		exit();
	}

?>