<?php require_once "../../ressources/Model/Model.php";
session_start();

if (isset($_GET['id']) && $_GET['id'] != $_SESSION['id_b']) {
	$m->deletePersonne($_GET['id']);
	header('Location: ./personneInactif.php');
	exit();
}

?>