<?php $title="Jeunes Ajouté"; require_once("../../ressources/Model/Model.php");
	
	if ( isset($_POST['nom'],
			   $_POST['prenom'],
			   $_POST['age'],
			   $_POST['genre'],
			   $_POST['autonomie'],
			   $_POST['dateArrivee'] )
		&& (isset($_POST['tel']) || isset($_POST['autrecontact']))
		)
	{
		// Information Personne
		$nom=htmlspecialchars($_POST['nom']);
		$prenom=htmlspecialchars($_POST['prenom']);
		$tel=htmlspecialchars($_POST['tel']);
		$autrecontact=htmlspecialchars($_POST['autrecontact']);
		$description=htmlspecialchars($_POST['description']);
		$remarque=htmlspecialchars($_POST['remarque']);

		// Tags
		$autonomie=htmlspecialchars($_POST['autonomie']);
		$genre=htmlspecialchars($_POST['genre']);
		$tags= [$genre,$autonomie];

		$infos_personne=[
						  'nom' => $nom,
						  'prenom' => $prenom,
						  'telephone' => $tel,
						  'autre_contact' => $autrecontact,
						  'description' => $description,
						  'remarque' => $remarque,
						  'tags' => $tags,
						  'actif' => true
		];

		// Information Mineur
		$age=htmlspecialchars($_POST['age']);
		$dateArrivee=htmlspecialchars($_POST['dateArrivee']);

		$infos_mineur=[
					   'age' => $age,
					   'heberge' => false,
					   'date_arrive' => $dateArrivee,
					   'genre'=>$genre
		];

		$m->addMineur($infos_personne, $infos_mineur);

		header('Location: ./jeunes.php');
  		exit();
	}
	else
	{
		header('Location: ./ajoutJeune.php');
  		exit();
	}

?>