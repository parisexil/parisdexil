
<?php $title="Modifier un Bénévole"; require_once("../header.php");
	  $jeunes=$m->getMineurById($_GET['id']);
	  $tags=unserialize(base64_decode($jeunes['tags'])); ?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/jeunes.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small"></span>
							<h1>Modifier un Jeune</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	
	<div class="gtco-section">
		<div>
			<form class="form" action="./modif.php" method="post">
				<input name="id" value="<?= $_GET['id'] ?>" hidden/>
				<input name="id_p" value="<?= $_GET['id_p'] ?>" hidden/>
				<label>Informations Personnelles</label>
				<hr>
				<div class="form-group">
					<br>
					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="nom">Nom <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="nom" name="nom" value="<?= $jeunes['nom'] ?>">
						</div>

						<div class="form-group col-md-5">
							<label for="prenom">Prénom <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="prenom" name="prenom" value="<?= $jeunes['prenom'] ?>">
						</div>
						<div class="form-group col-md-2">
							<label for="age">Age <span class="obligatoire">*</span></label>
							<input type="text" class="form-control" id="age" name="age" value="<?= $jeunes['age'] ?>">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-2">
							<label for="Genre">Genre <span class="obligatoire">*</span></label>
							<select id="Genre" name="genre" class="form-control">
								<option>Choisir ...</option>
								<option <?= $jeunes['genre']=="1" ? "selected" : "" ?>>Garçon</option>
								<option <?= $jeunes['genre']=="0" ? "selected" : "" ?>>Fille</option>
							</select>
						</div>

						<div class="form-group col-md-4">
							<label for="tel">Téléphone</label>
							<input type="tel" class="form-control" id="tel" name="tel" value="<?= $jeunes['telephone'] ?>">
						</div>

						<div class="form-group col-md-6">
							<label for="autrecontact">Autre Contact ( <span class="obligatoire">*</span> si pas de téléphone)</label>
							<input type="text" class="form-control" id="autrecontact" name="autrecontact" value="<?= $jeunes['autre_contact'] ?>">
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="description">Description</label>
							<textarea id="description" class="form-control" name="description" rows="5"><?= $jeunes['description'] ?></textarea>
						</div>
					</div>

					<br>
					<div class="form-row">
						<div class="form-check">
						  	<input class="form-check-input position-static" type="radio" name="autonomie" id="autonome" value="Autonome" <?= $tags[1]=="Autonome" ? "checked" : "" ?>>
						  	<label class="form-check-label" for="autonome">
						    	Autonome
						  	</label>
						</div>
						<div class="form-check">
						  	<input class="form-check-input position-static" type="radio" name="autonomie" id="nonAutonome" value="Non Autonome" <?= $tags[1]=="Non Autonome" ? "checked" : "" ?>>
						  	<label class="form-check-label" for="nonAutonome">
						    	Non Autonome
						  	</label>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="datearrivee">Date d'arrivée <span class="obligatoire">*</span></label>
							<input type="date" class="form-control" id="datearrivee" name="dateArrivee" value="<?= $jeunes['date_arrive'] ?>">
						</div>
					</div>
					<br>
				</div>
				<br>

				<label>Informations Supplémentaires</label>
				<hr>
				<div class="form-group">
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="remarque">Remarques</label>
							<textarea id="remarque" class="form-control" name="remarque" rows="5" ><?= $jeunes['remarque'] ?></textarea>
						</div>
					</div>
			  	</div>
			  	<br>
			  	<div class="form-row">
					<button type="submit" class="btn btn-primary">Modifier</button>
				</div>
			</form>
		</div>
	</div>

<?php require_once("../footer.html") ;?>