<?php require_once("../../ressources/Model/Model.php");
	
	if ( isset($_POST['nom'],
			   $_POST['prenom'],
			   $_POST['mail'],
			   $_POST['identifiant'])
		&& (isset($_POST['tel']) || isset($_POST['autrecontact']))
		)
	{
		// Information Personne
		$id_p=htmlspecialchars($_POST['id_p']);
		$nom=htmlspecialchars($_POST['nom']);
		$prenom=htmlspecialchars($_POST['prenom']);
		$genre=htmlspecialchars($_POST['genre']);
		$tel=htmlspecialchars($_POST['tel']);
		$autrecontact=htmlspecialchars($_POST['autrecontact']);
		$description=htmlspecialchars($_POST['description']);
		$remarque=htmlspecialchars($_POST['remarque']);


		$infos_personne=[
						  'id' => $id_p,
						  'nom' => $nom,
						  'prenom' => $prenom,
						  'telephone' => $tel,
						  'autre_contact' => $autrecontact,
						  'description' => $description,
						  'remarque' => $remarque,
						  'tags' => [],
						  'actif' => true
		];

		// Information benevole
		$id=htmlspecialchars($_POST['id']);
		$mail=htmlspecialchars($_POST['mail']);
		$identifiant=htmlspecialchars($_POST['identifiant']);
		if ($_POST['mdp']=="")
		{
			$mdp=password_hash($_POST['Amdp'], PASSWORD_ARGON2I);
		}
		else
		{
			$mdp=password_hash($_POST['mdp'], PASSWORD_ARGON2I);
		}
		
		$admin=htmlspecialchars($_POST['admin']);

		$infos_benevole=[
						   'id' => $id,
						   'mail' => $mail,
						   'identifiant' => $identifiant,
						   'mdp' => $mdp,
						   'admin' => $admin
		];

		$m->updateBenevole($infos_personne, $infos_benevole);

		header('Location: ./benevoles.php');
  		exit();
	}
	else
	{
		header('Location: ./modifBenevole.php?id='.$_POST['id'].'&id_p='.$_POST['id_p']);
  		exit();
	}

?>