
<?php $title="Ajouter un Transfert"; require_once("../header.php");
	  $jeunes= $m->getAllMineurs();
	  $hebergeurs= $m->getAllHebergeurs(); ?>

	
	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/img_4.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small"></span>
							<h1>Ajout d'un Transfert</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	<div class="gtco-section">
		<div>
			<form class="form" action="./ajout.php" method="post">
				<label>Jeune</label>
				<hr>
				<div class="form-group">
					<br>
					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="jeunes">Nom du Jeune<span class="obligatoire">*</span></label>
							<select id="jeunes" class="form-control" name="id_jeune">
								<option >Choisir ...</option>
								<?php for($i=0; $i<sizeof($jeunes); $i++) : ?>
									<option value="<?= $jeunes[$i]['id'] ?>" > <?= $jeunes[$i]['nom'] ?> </option>
								<?php endfor; ?>
							</select>
						</div>
					</div>
				</div>
				<br>

				<label>Hébergeur</label>
				<hr>
				<div class="form-group">
					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="hebergeur">Nom du Hébergeur<span class="obligatoire">*</span></label>
							<select id="hebergeur" class="form-control" name="id_hebergeur">
								<option >Choisir ...</option>
								<?php for($j=0; $j<sizeof($hebergeurs); $j++) : ?>
									<option value="<?= $hebergeurs[$j]['id'] ?>"> <?= $hebergeurs[$j]['nom'] ?> </option>
								<?php endfor; ?>
							</select>
						</div>
					</div>
			  	</div>
			  	<br>
			  	<br>

				<label>Transfert</label>
				<hr>
				<div class="form-group">
					<div class="form-row">
						<div class="form-group col-md-2">
							<label for="dateDebut">Date de début <span class="obligatoire">*</span></label>
							<input type="date" class="form-control" id="dateDebut" name="dateDebut">
						</div>
					</div>
					<br>
					<div class="form-row">
						<div class="form-group col-md-2">
							<label for="dateFin">Date de fin (si connue)</label>
							<input type="date" class="form-control" id="dateFin" name="dateFin">
						</div>
					</div>
					<br>
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="status">Status du Transfert<span class="obligatoire">*</span></label>
							<select id="status" class="form-control" name="status">
								<option >Choisir ...</option>
								<option value="enCours">En cours</option>
								<option value="aVenir">A venir</option>
								<option value="annulé">Annulé</option>
								<option value="terminé">Terminé</option>
							</select>
						</div>
					</div>
			  	</div>
			  	<br>
			  	<br>

			  	<div class="form-row">
					<button type="submit" class="btn btn-primary">Ajouter</button>
				</div>
			</form>
		</div>
	</div>

<?php require_once("../footer.html") ;?>