
DROP TABLE IF EXISTS `benevole`;
CREATE TABLE IF NOT EXISTS `benevole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num_personne_id` int(11) NOT NULL,
  `date_ajout` datetime NOT NULL,
  `mail` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identifiant` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mdp` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B4014FDBD8FE9CFA` (`num_personne_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `hebergeur`;
CREATE TABLE IF NOT EXISTS `hebergeur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num_personne_id` int(11) NOT NULL,
  `date_ajout` datetime NOT NULL,
  `adresse` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_postal` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ville` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disponibilite` longtext COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:array)',
  `description_endroit` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5A15B295D8FE9CFA` (`num_personne_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `mineur`;
CREATE TABLE IF NOT EXISTS `mineur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num_personne_id` int(11) NOT NULL,
  `date_ajout` datetime NOT NULL,
  `age` smallint(6) NOT NULL,
  `heberge` tinyint(1) NOT NULL,
  `date_arrive` date DEFAULT NULL,
  `genre` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E82CBC43D8FE9CFA` (`num_personne_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `personne`;
CREATE TABLE IF NOT EXISTS `personne` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_ajout` datetime NOT NULL,
  `nom` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `autre_contact` longtext COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `remarque` longtext COLLATE utf8mb4_unicode_ci,
  `tags` longtext COLLATE utf8mb4_unicode_ci COMMENT '(DC2Type:array)',
  `actif` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `transfert`;
CREATE TABLE IF NOT EXISTS `transfert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num_hebergeur_id` int(11) NOT NULL,
  `num_mineur_id` int(11) NOT NULL,
  `date_ajout` datetime NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `etat` varchar(18) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1E4EACBB881C18A7` (`num_hebergeur_id`),
  KEY `IDX_1E4EACBB1CEB0FCA` (`num_mineur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `benevole`
  ADD CONSTRAINT `FK_B4014FDBD8FE9CFA` FOREIGN KEY (`num_personne_id`) REFERENCES `personne` (`id`);

ALTER TABLE `hebergeur`
  ADD CONSTRAINT `FK_5A15B295D8FE9CFA` FOREIGN KEY (`num_personne_id`) REFERENCES `personne` (`id`);
ALTER TABLE `mineur`
  ADD CONSTRAINT `FK_E82CBC43D8FE9CFA` FOREIGN KEY (`num_personne_id`) REFERENCES `personne` (`id`);

ALTER TABLE `transfert`
  ADD CONSTRAINT `FK_1E4EACBB1CEB0FCA` FOREIGN KEY (`num_mineur_id`) REFERENCES `mineur` (`id`),
  ADD CONSTRAINT `FK_1E4EACBB881C18A7` FOREIGN KEY (`num_hebergeur_id`) REFERENCES `hebergeur` (`id`);
COMMIT;
