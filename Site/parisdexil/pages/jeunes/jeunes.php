
<?php $title="Jeunes"; require_once("../header.php");
	  $jeunes=$m->getAllMineurs(); ?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/img_4.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Jeunes</span>
							<h1>Les Jeunes</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	
	<div id="gtco-features" class="border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-12 col-md-8">
					<p><a href="./ajoutJeune.php" class="btn btn-default btn-block">Ajouter un jeune</a></p>
				</div>
				<div class="col-6 col-md-4">
				    <form class="text-center" role="search"> 
					    <div class="form-inline"> 
					    	<input type="search" class="form-control mr-sm-2" placeholder="Search" /> 
					     	<span class="input-group-btn"> 
					      		<button type="button" class="btn btn-outline my-2 my-sm-0"><i class="fas fa-search"></i></button> 
					     	</span> 
					    </div> 
				    </form> 
			    </div>
			</div>
			
			<div class="row">
				<?php for($i=0; $i<sizeof($jeunes); $i++): ?>

				<div class="col-md-3 col-sm-6">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<span class="icon">
							<img src="/parisdexil/ressources/images/no-image.jpg" height="200" width="175"/>
						</span>
						<h3><?= $jeunes[$i]['nom']." ".$jeunes[$i]['prenom']?></h3>
						<p>
							<ul class="text-left">
								<li>Age : <?= $jeunes[$i]['age'] ?></li>
								<li>Date d'arrivée : <?= $jeunes[$i]['date_arrive'] ?></li>
							</ul>
						</p>
						<hr>

						<!-- ajout Tags-->
						<?php foreach(unserialize(base64_decode($jeunes[$i]['tags'])) as $val): ?>
							<?php if ($val=="Garçon") : ?>
								<span class="badge badge-info"><?= $val ?></span>
							<?php endif; ?>

							<?php if ($val=="Fille") : ?>
								<span class="badge badge-danger"><?= $val ?></span>
							<?php endif; ?>

							<?php if ($val=="Autonome") : ?>
								<span class="badge badge-success"><?= $val ?></span>
							<?php endif; ?>

							<?php if ($val=="Non Autonome") : ?>
								<span class="badge badge-warning"><?= $val ?></span>
							<?php endif; ?>
						<?php endforeach; ?>
						<hr>
						<p><a href="./infoJeune.php?id=<?= $jeunes[$i]['id'] ?>" class="btn btn-default btn-block">Plus d'informations</a></p>
					</div>
				</div>

				<?php endfor; ?>
				
			</div>

			<nav aria-label="Page navigation example" class="col-lg-6 offset-lg-3 py-5">
			  	<ul class="pagination">
			    	<li class="page-item disabled">
				      	<a class="page-link" href="#" aria-label="Previous">
				        	<span aria-hidden="true">&laquo;</span>
				      	</a>
			   		</li>
				    <li class="page-item active"><a class="page-link" href="#">1</a></li>
				    <li class="page-item"><a class="page-link" href="#">2</a></li>
				    <li class="page-item"><a class="page-link" href="#">3</a></li>
				    <li class="page-item">
					    <a class="page-link" href="#" aria-label="Next">
					    	<span aria-hidden="true">&raquo;</span>
					    </a>
			    	</li>
			  	</ul>
			</nav>
			
		</div>
	</div>

<?php require_once("../footer.html") ;?>