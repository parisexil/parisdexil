<?php
session_start();

if (isset($_SESSION['connected'])) {
	if (!$_SESSION['connected']) {
		echo "test";

		if (isset($_POST['username']) and isset($_POST['password'])) {
			$username = $_POST['username'];
			$password = $_POST['password'];
		} else {
			header('Location: http://localhost/parisdexil/index.html');
			exit();
		}

		$login = $m->login($username, $password);

		if ($login['valide']) {
			$info = $m->getBenevoleById($login['id_b']);
			$_SESSION['connected'] = true;
			$_SESSION['nom'] = $info['nom'];
			$_SESSION['prenom'] = $info['prenom'];
			$_SESSION['id_b'] = $login['id_b'];
			$_SESSION['admin'] = ($info['admin'] == 0);

		} else {
			header('Location: http://localhost/parisdexil/index.html');
			exit();
		}
	}

} else {
	$_SESSION['connected'] = false;
}

?>
