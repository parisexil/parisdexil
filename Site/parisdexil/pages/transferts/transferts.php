
<?php $title="Transferts"; require_once("../header.php");
	  $transferts=$m->getAllTransfert();
	  var_dump($transferts); ?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/transf.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Transferts</span>
							<h1>Les transferts</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	
	<div id="gtco-features" class="border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8">
					<p><a href="./ajoutTransfert.php" class="btn btn-default btn-block">Nouveau tranfert</a></p>
				</div>
				<div class="col-6 col-md-4">
					    <form class="text-center" role="search"> 
						    <div class="form-inline"> 
						    	<input type="text" class="form-control mr-sm-2" placeholder="Search" /> 
						     	<span class="input-group-btn"> 
						      		<button type="button" class="btn btn-outline my-2 my-sm-0"><i class="fas fa-search"></i></button> 
						     	</span> 
						    </div> 
					    </form> 
			    </div>
			</div>

			<?php for($i=0; $i<sizeof($transferts); $i++ ): ?> 
						
			<div class="transfert">
				<a href="" >
					<div class="row">
						<div class="col-sm">
							<div class="feature-center animate-box" data-animate-effect="fadeIn">
								<!-- <span class="icon">
									<img src="/parisdexil/ressources/images/no-image.jpg" height="200" width="175"/>
								</span> -->
								<h3><?= $transferts[$i]['nom_m']." ".$transferts[$i]['prenom_m'] ?></h3>
							</div>
						</div>
						<div class="col-sm">
							<div class="feature-center animate-box" data-animate-effect="fadeIn">
								<span class="icon">
									<i class="fas fa-exchange-alt fa-7x" style="height: 200px;width: 175px"></i>
								</span>
								<p>
									<ul class="text-left">
										<li>Date de début : <?= $transferts[$i]['date_debut'] ?></li>
										<li>Date de fin : <?= $transferts[$i]['date_fin'] ?></li>
										<li>Status :  
											<?= $transferts[$i]['etat']=="terminé" ? "<span class=\"badge badge-success\" style=\"color:white\">Terminé</span>" : "" ?>
											<?= $transferts[$i]['etat']=="annulé" ? "<span class=\"badge badge-danger\" style=\"color:white\">Annulé</span>" : "" ?>
											<?= $transferts[$i]['etat']=="enCours" ? "<span class=\"badge badge-primary\" style=\"color:white\">En cours</span>" : "" ?>
											<?= $transferts[$i]['etat']=="aVenir" ? "<span class=\"badge badge-info\" style=\"color:white\">A venir</span>" : "" ?>
										</li>
									</ul>
									<br>
								</p>
							</div>
						</div>
						<div class="col-sm">
							<div class="feature-center animate-box" data-animate-effect="fadeIn">
								<!-- <span class="icon">
									<img src="/parisdexil/ressources/images/no-image.jpg" height="200" width="175"/>
								</span> -->
								<h3><?= $transferts[$i]['nom_h']." ".$transferts[$i]['prenom_h'] ?></h3>
							</div>
						</div>
					</div>
					<hr>
					<div class="row text-center">
						<p class="col-sm-6"><a href="./modifTransfert.php?id=<?= $transferts[$i]['id'] ?>" class="btn btn-default btn-block">Modifier</a></p>
						<p class="col-md-6"><a href="./suppTransfert.php?id=<?= $transferts[$i]['id'] ?>" class="btn btn-danger btn-block">Supprimer</a></p>
					</div>
				</a>
			</div>

			<hr>

			<?php endfor; ?>

			<nav aria-label="Page navigation example" class="col-lg-6 offset-lg-3 py-5">
			  	<ul class="pagination">
			    	<li class="page-item disabled">
				      	<a class="page-link" href="#" aria-label="Previous">
				        	<span aria-hidden="true">&laquo;</span>
				      	</a>
			   		</li>
				    <li class="page-item active"><a class="page-link" href="#">1</a></li>
				    <li class="page-item"><a class="page-link" href="#">2</a></li>
				    <li class="page-item"><a class="page-link" href="#">3</a></li>
				    <li class="page-item">
					    <a class="page-link" href="#" aria-label="Next">
					    	<span aria-hidden="true">&raquo;</span>
					    </a>
			    	</li>
			  	</ul>
			</nav>
		</div>
	</div>

<?php require_once("../footer.html") ;?>
