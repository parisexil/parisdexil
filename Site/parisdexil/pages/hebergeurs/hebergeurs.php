
<?php $title="Hébergeurs"; require_once("../header.php");
	  $hebergeurs=$m->getAllHebergeurs(); ?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/house.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Hébergeurs</span>
							<h1>Les Hébergeurs</h1>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	
	<div id="gtco-features" class="border-bottom">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8">
					<p><a href="./ajoutHebergeur.php" class="btn btn-default btn-block">Ajouter un hébergeur</a></p>
				</div>
				<div class="col-6 col-md-4">
				    <form class="text-center" role="search"> 
					    <div class="form-inline"> 
					    	<input type="text" class="form-control mr-sm-2" placeholder="Search" /> 
					     	<span class="input-group-btn"> 
					      		<button type="button" class="btn btn-outline my-2 my-sm-0"><i class="fas fa-search"></i></button> 
					     	</span> 
					    </div> 
				    </form> 
			    </div>
			</div>
			<div class="row">

				<?php for($i=0; $i<sizeof($hebergeurs); $i++): ?>
					<div class="col-md-3 col-sm-6">
						<div class="feature-center animate-box" data-animate-effect="fadeIn">
							<span class="icon">
								<img src="/parisdexil/ressources/images/no-image.jpg" height="200" width="175"/>
							</span>
							<h3><?= $hebergeurs[$i]['nom']." ".$hebergeurs[$i]['prenom'] ?></h3>
							<hr>
							<?php foreach(unserialize(base64_decode($hebergeurs[$i]['tags'])) as $val): ?>
								<span class="badge badge-info"><?= $val ?></span>
							<?php endforeach;?>
							<hr>
							<p><a href="./infohebergeur.php?id=<?= $hebergeurs[$i]['id'] ?>" class="btn btn-default btn-block">Plus d'informations</a></p>
						</div>
					</div>
				<?php endfor;?>
			</div>

			<nav aria-label="Page navigation example" class="col-lg-6 offset-lg-3 py-5">
			  	<ul class="pagination">
			    	<li class="page-item disabled">
				      	<a class="page-link" href="#" aria-label="Previous">
				        	<span aria-hidden="true">&laquo;</span>
				      	</a>
			   		</li>
				    <li class="page-item active"><a class="page-link" href="#">1</a></li>
				    <li class="page-item"><a class="page-link" href="#">2</a></li>
				    <li class="page-item"><a class="page-link" href="#">3</a></li>
				    <li class="page-item">
					    <a class="page-link" href="#" aria-label="Next">
					    	<span aria-hidden="true">&raquo;</span>
					    </a>
			    	</li>
			  	</ul>
			</nav>

		</div>
	</div>
	
<?php require_once("../footer.html") ;?>