
<?php $title = "Informations Bénévole";require_once "../header.php";
$benevoles = $m->getBenevoleById($_GET['id']);?>

	<header id="gtco-header" class="gtco-cover gtco-cover-sm" role="banner" style="background-image: url('/parisdexil/ressources/images/benevoles.jpg')">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-left">
					<div class="row row-mt-15em">
						<div class="col-md-7 mt-text animate-box" data-animate-effect="fadeInUp">
							<span class="intro-text-small">Informations Bénévole</span>
							<h1><?=$benevoles['nom'] . " " . $benevoles['prenom']?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>


	<div id="gtco-features" class="border-bottom">
		<div class="gtco-container">
			<div class="info">
				<div class="row">
					<div class="col-sm">
						<div class="feature-center animate-box" data-animate-effect="fadeIn">
							<span class="icon">
								<img src="/parisdexil/ressources/images/photo-benevole.png" height="200" width="175"/>
							</span>
							<h3><?=$benevoles['nom'] . " " . $benevoles['prenom']?></h3>
							<p>
								<ul class="text-left">
									<li>Droit : <span><?=$benevoles['admin'] == "1" ? "Admin" : "Aucun"?></span> </li>
									<li>Télephone : <span><?=$benevoles['telephone']?></span> </li>
									<li>Autre Contact : <span><?=$benevoles['autre_contact']?></span> </li>
									<li>Identifiant : <span><?=$benevoles['identifiant']?></span> </li>
									<li>Mail : <span><?=$benevoles['mail']?></span> </li>
									<li>Description : <span><?=$benevoles['description']?></span> </li>
									<li>Date d'ajout : <span><?=$benevoles['date_ajout']?></span> </li>
									<li>Remarque : <span><?=$benevoles['remarque']?></span> </li>
								</ul>
							</p>
							<hr>
							<div class="form-row">
								<p class="col-sm-6 "><a href="./modifBenevole.php?id=<?=$benevoles['id']?>&id_p=<?=$benevoles['num_personne_id']?>" class="btn btn-default btn-block">Modifier</a></p>
								<p class="col-md-6 	"><a href="./suppBenevole.php?id=<?=$benevoles['num_personne_id']?>" class="btn btn-danger btn-block">Supprimer</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php require_once '../footer.html'?>